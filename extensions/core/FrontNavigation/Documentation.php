<?php
/**
 * @brief		Front Navigation Extension: Documentation
 * @author		<a href='http://www.invisionpower.com'>Invision Power Services, Inc.</a>
 * @copyright	(c) 2001 - SVN_YYYY Invision Power Services, Inc.
 * @license		http://www.invisionpower.com/legal/standards/
 * @package		IPS Social Suite
 * @subpackage	
 * @since		10 Dec 2013
 * @version		SVN_VERSION_NUMBER
 */

namespace IPS\Documentation\extensions\core\FrontNavigation;

/**
 * Front Navigation Extension: Documentation
 */
class _Documentation extends \IPS\core\FrontNavigation\FrontNavigationAbstract
{
	/**
	 * Can access?
	 *
	 * @return	bool
	 */
	public function canView()
	{
		return !\IPS\Application::load('documentation')->hide_tab and \IPS\Member::loggedIn()->canAccessModule( \IPS\Application\Module::get( 'documentation', 'documentation' ) );
	}
	
	/**
	 * Get Title
	 *
	 * @return	string
	 */
	public function title()
	{
		return \IPS\Member::loggedIn()->language()->addToStack('__app_documentation');
	}
	
	/**
	 * Get Link
	 *
	 * @return	\IPS\Http\Url
	 */
	public function link()
	{
		return \IPS\Http\Url::internal( "app=documentation&module=documentation&controller=home", 'front' );
	}
	
	/**
	 * Is Active?
	 *
	 * @return	bool
	 */
	public function active()
	{
		return \IPS\Dispatcher::i()->application->directory === 'documentation';
	}
	
	/**
	 * Children
	 *
	 * @return	array
	 */
	public function children($noStore = false)
	{
		return array();
	}
}