<?php


namespace IPS\documentation\modules\front\documentation;

class _css extends \IPS\Dispatcher\Controller
{
	public function execute()
	{
		\IPS\Output::i()->sidebar['enabled'] = FALSE;
		//\IPS\Output::i()->bodyClasses[] = 'ipsLayout_minimal';
		\IPS\Output::i()->cssFiles = array_merge( \IPS\Output::i()->cssFiles, \IPS\Theme::i()->css( 'front_.css', 'documentation' ) );
		parent::execute();
	}

	protected function manage()
	{
		$content = \IPS\Theme::i()->getTemplate('css', 'documentation', 'front')->home();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('css', 'documentation', 'front')->wrapper( $content, '' );
		}
	}

	protected function buttons()
	{
		\IPS\Output::i()->title = "Buttons";
		$content = \IPS\Theme::i()->getTemplate('css', 'documentation', 'front')->buttons();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('css', 'documentation', 'front')->wrapper( $content, 'buttons' );
		}
	}

	protected function columns()
	{
		\IPS\Output::i()->title = "Layout: Columns";
		$content = \IPS\Theme::i()->getTemplate('css', 'documentation', 'front')->columns();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('css', 'documentation', 'front')->wrapper( $content, 'columns' );
		}
	}

	protected function datalists()
	{
		\IPS\Output::i()->title = "Data Lists";
		$content = \IPS\Theme::i()->getTemplate('css', 'documentation', 'front')->datalists();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('css', 'documentation', 'front')->wrapper( $content, 'datalists' );
		}
	}

	protected function forms()
	{
		\IPS\Output::i()->title = "Forms";
		$content = \IPS\Theme::i()->getTemplate('css', 'documentation', 'front')->forms();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('css', 'documentation', 'front')->wrapper( $content, 'forms' );
		}
	}

	protected function icons()
	{
		\IPS\Output::i()->title = "Icons";
		$content = \IPS\Theme::i()->getTemplate('css', 'documentation', 'front')->icons();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('css', 'documentation', 'front')->wrapper( $content, 'icons' );
		}
	}

	protected function grids()
	{
		\IPS\Output::i()->title = "Layout: Grids";
		$content = \IPS\Theme::i()->getTemplate('css', 'documentation', 'front')->grids();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('css', 'documentation', 'front')->wrapper( $content, 'grids' );
		}
	}

	protected function images()
	{
		\IPS\Output::i()->title = "Images &amp; Photos";
		$content = \IPS\Theme::i()->getTemplate('css', 'documentation', 'front')->images();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('css', 'documentation', 'front')->wrapper( $content, 'images' );
		}
	}

	protected function menus()
	{
		\IPS\Output::i()->title = "Dropdown Menus";
		$content = \IPS\Theme::i()->getTemplate('css', 'documentation', 'front')->menus();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('css', 'documentation', 'front')->wrapper( $content, 'menus' );
		}
	}

	protected function messages()
	{
		\IPS\Output::i()->title = "Messages";
		$content = \IPS\Theme::i()->getTemplate('css', 'documentation', 'front')->messages();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('css', 'documentation', 'front')->wrapper( $content, 'messages' );
		}
	}

	protected function misc()
	{
		\IPS\Output::i()->title = "Miscellaneous";
		$content = \IPS\Theme::i()->getTemplate('css', 'documentation', 'front')->misc();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('css', 'documentation', 'front')->wrapper( $content, 'misc' );
		}
	}

	protected function naming()
	{
		\IPS\Output::i()->title = "CSS &amp; HTML Naming Conventions";
		$content = \IPS\Theme::i()->getTemplate('css', 'documentation', 'front')->naming();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('css', 'documentation', 'front')->wrapper( $content, 'naming' );
		}
	}

	protected function sidemenus()
	{
		\IPS\Output::i()->title = "Side Menus";
		$content = \IPS\Theme::i()->getTemplate('css', 'documentation', 'front')->naming();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('css', 'documentation', 'front')->wrapper( $content, 'naming' );
		}
	}

	protected function tables()
	{
		\IPS\Output::i()->title = "Tables";
		$content = \IPS\Theme::i()->getTemplate('css', 'documentation', 'front')->tables();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('css', 'documentation', 'front')->wrapper( $content, 'tables' );
		}
	}

	protected function type()
	{
		\IPS\Output::i()->title = "Typography";
		$content = \IPS\Theme::i()->getTemplate('css', 'documentation', 'front')->type();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('css', 'documentation', 'front')->wrapper( $content, 'type' );
		}
	}

	

	

}