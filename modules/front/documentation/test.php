<?php


namespace IPS\documentation\modules\front\documentation;

class _test extends \IPS\Dispatcher\Controller
{
	protected function manage()
	{
		\IPS\Output::i()->bodyClasses[] = 'ipsLayout_minimal';
		
		$form = new \IPS\Helpers\Form;
		$form->class = 'ipsForm_vertical';
		$form->add( new \IPS\Helpers\Form\Editor( 'contact_text', NULL, TRUE, array(
				'app'			=> 'core',
				'key'			=> 'Admin',
				'autoSaveKey'	=> 'test',
				'minimize'		=> 'x',
		) ) );
		if ( $values = $form->values() )
		{
			
		}
		
		\IPS\Output::i()->output = $form;
	}

}