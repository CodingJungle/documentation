<?php


namespace IPS\documentation\modules\front\documentation;

class _home extends \IPS\Dispatcher\Controller
{
	protected function manage()
	{
		\IPS\Output::i()->sidebar['enabled'] = FALSE;
		\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('documentation')->home();
	}

}