<?php


namespace IPS\documentation\modules\front\documentation;

class _javascript extends \IPS\Dispatcher\Controller
{
	public function execute()
	{
		\IPS\Output::i()->sidebar['enabled'] = FALSE;
		//\IPS\Output::i()->bodyClasses[] = 'ipsLayout_minimal';
		\IPS\Output::i()->jsFiles = array_merge( \IPS\Output::i()->jsFiles, \IPS\Output::i()->js( 'jquery/jquery-ui.js', 'core', 'interface' ) );
		\IPS\Output::i()->jsFiles = array_merge( \IPS\Output::i()->jsFiles, \IPS\Output::i()->js( 'jquery/jquery-touchpunch.js', 'core', 'interface' ) );
		\IPS\Output::i()->cssFiles = array_merge( \IPS\Output::i()->cssFiles, \IPS\Theme::i()->css( 'front_.css', 'documentation' ) );
		parent::execute();
	}

	/*******************************************************************/
	/* GENERAL */
	/*******************************************************************/
	protected function manage()
	{
		\IPS\Output::i()->title = "Home";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->home();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, '' );
		}
	}

	protected function standards()
	{
		\IPS\Output::i()->title = "Coding Standards";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->standards();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'standards' );
		}
	}

	/*******************************************************************/
	/* FRAMEWORK */
	/*******************************************************************/

	protected function overview()
	{
		\IPS\Output::i()->title = "Overview of the Javascript Framework";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->overview();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'overview' );
		}
	}

	protected function files()
	{
		\IPS\Output::i()->title = "File Structure";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->files();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'files' );
		}
	}

	protected function modules()
	{
		\IPS\Output::i()->title = "Building Modules";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->modules();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'modules' );
		}
	}

	protected function controllers()
	{
		\IPS\Output::i()->title = "Controllers";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->controllers();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'controllers' );
		}
	}

	protected function widgets()
	{
		\IPS\Output::i()->title = "Creating UI Widgets";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->widgets();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'widgets' );
		}
	}

	protected function templates()
	{
		\IPS\Output::i()->title = "Templates";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->templates();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'templates' );
		}
	}

	protected function languages()
	{
		\IPS\Output::i()->title = "Language Strings";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->languages();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'languages' );
		}
	}

	protected function loading()
	{
		\IPS\Output::i()->title = "Loading Content";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->loading();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'loading' );
		}
	}

	/*******************************************************************/
	/* WIDGETS */
	/*******************************************************************/

	protected function usingwidgets()
	{
		\IPS\Output::i()->title = "Using the Data API";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->usingwidgets();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'usingwidgets' );
		}
	}

	protected function uimenu()
	{
		\IPS\Output::i()->title = "Menu Widget";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->uimenu();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'uimenu' );
		}
	}

	protected function uialert()
	{
		\IPS\Output::i()->title = "Alert Widget";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->uialert();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'uialert' );
		}
	}

	protected function uiautocheck()
	{
		\IPS\Output::i()->title = "AutoCheck Widget";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->uiautocheck();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'uiautocheck' );
		}
	}

	protected function uiautocomplete()
	{
		\IPS\Output::i()->title = "AutoComplete Widget";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->uiautocomplete();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'uiautocomplete' );
		}
	}

	protected function uicaptcha()
	{
		\IPS\Output::i()->title = "Captcha Widget";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->uicaptcha();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'uicaptcha' );
		}
	}

	protected function uichart()
	{
		\IPS\Output::i()->title = "Chart Widget";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->uichart();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'uichart' );
		}
	}

	protected function uidialog()
	{
		\IPS\Output::i()->title = "Dialog Widget";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->uidialog();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'uidialog' );
		}
	}

	protected function uiflashmsg()
	{
		\IPS\Output::i()->title = "FlashMsg Widget";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->uiflashmsg();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'uiflashmsg' );
		}
	}

	protected function uiform()
	{
		\IPS\Output::i()->title = "Form Widget";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->uiform();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'uiform' );
		}
	}

	protected function uihovercard()
	{
		\IPS\Output::i()->title = "Hovercard Widget";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->uihovercard();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'uihovercard' );
		}
	}

	protected function uiinfinitescroll()
	{
		\IPS\Output::i()->title = "InfiniteScroll Widget";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->uiinfinitescroll();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'uiinfinitescroll' );
		}
	}

	protected function uilightbox()
	{
		\IPS\Output::i()->title = "Lightbox Widget";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->uilightbox();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'uilightbox' );
		}
	}

	protected function uipageaction()
	{
		\IPS\Output::i()->title = "PageAction Widget";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->uipageaction();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'uipageaction' );
		}
	}

	protected function uipagination()
	{
		\IPS\Output::i()->title = "Pagination Widget";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->uipagination();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'uipagination' );
		}
	}

	protected function uiquote()
	{
		\IPS\Output::i()->title = "Quote Widget";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->uiquote();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'uiquote' );
		}
	}

	protected function uirating()
	{
		\IPS\Output::i()->title = "Rating Widget";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->uirating();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'uirating' );
		}
	}

	protected function uisidemenu()
	{
		\IPS\Output::i()->title = "SideMenu Widget";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->uisidemenu();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'uisidemenu' );
		}
	}

	protected function uispoiler()
	{
		\IPS\Output::i()->title = "Spoiler Widget";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->uispoiler();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'uispoiler' );
		}
	}

	protected function uisticky()
	{
		\IPS\Output::i()->title = "Sticky Widget";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->uisticky();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'uisticky' );
		}
	}

	protected function uistack()
	{
		\IPS\Output::i()->title = "Stack Widget";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->uistack();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'uistack' );
		}
	}

	protected function uitabbar()
	{
		\IPS\Output::i()->title = "TabBar Widget";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->uitabbar();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'uitabbar' );
		}
	}

	protected function uitoggle()
	{
		\IPS\Output::i()->title = "Toggle Widget";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->uitoggle();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'uitoggle' );
		}
	}

	protected function uitooltip()
	{
		\IPS\Output::i()->title = "Tooltip Widget";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->uitooltip();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'uitooltip' );
		}
	}

	protected function uitruncate()
	{
		\IPS\Output::i()->title = "PageAction Widget";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->uitruncate();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'uitruncate' );
		}
	}

	protected function uiwizard()
	{
		\IPS\Output::i()->title = "Wizard Widget";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->uiwizard();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'uiwizard' );
		}
	}

	/*******************************************************************/
	/* UTILITIES */
	/*******************************************************************/

	protected function utilanim()
	{
		\IPS\Output::i()->title = "Animation Utility";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->utilanim();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'utilanim' );
		}
	}

	protected function utilcookie()
	{
		\IPS\Output::i()->title = "Cookie Utility";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->utilcookie();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'utilcookie' );
		}
	}

	protected function utildb()
	{
		\IPS\Output::i()->title = "Local DB Utility";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->utildb();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'utildb' );
		}
	}

	protected function utilposition()
	{
		\IPS\Output::i()->title = "Position Utility";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->utilposition();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'utilposition' );
		}
	}

	protected function utiltime()
	{
		\IPS\Output::i()->title = "Time Utility";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->utiltime();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'utiltime' );
		}
	}

	protected function utilurl()
	{
		\IPS\Output::i()->title = "URL Utility";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->utilurl();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'utilurl' );
		}
	}

	/*******************************************************************/
	/* OTHER */
	/*******************************************************************/

	protected function otheracp()
	{
		\IPS\Output::i()->title = "AdminCP Shortcuts";
		$content = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->otheracp();

		if( \IPS\Request::i()->isAjax() ){
			\IPS\Output::i()->output = $content;
		} else {
			\IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('javascript', 'documentation', 'front')->wrapper( $content, 'otheracp' );
		}
	}
}