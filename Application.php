<?php
/**
 * @brief		Documentation Application Class
 * @author		<a href='http://www.invisionpower.com'>Invision Power Services, Inc.</a>
 * @copyright	(c) 2013 Invision Power Services, Inc.
 * @package		IPS Social Suite
 * @subpackage	Documentation
 * @since		10 Dec 2013
 * @version		
 */
 
namespace IPS\documentation;

/**
 * Documentation Application Class
 */
class _Application extends \IPS\Application
{

}