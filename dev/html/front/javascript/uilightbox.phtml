<ips:template parameters="" />

<div class='ipsPageHeader ipsAreaBackground ipsPad ipsClearfix ipsSpacer_bottom'>
	<h1 class='ipsType_pageTitle'>Lightbox Widget</h1>
	<p class='ipsPageHeader_info'>
		Displays images in an interactive popup over the page
	</p>
</div>
<br>

<article class='ipsType_normal ipsType_richText'>

	<strong>In this section:</strong>
	<ul class='ipsList_inline'>
		<li><a href='#description'>Description</a></li>
		<li><a href='#example'>Example</a></li>
		<li><a href='#usage'>Usage</a></li>
		<li><a href='#options'>Options</a></li>
		<li><a href='#events'>Events</a></li>
	</ul>
	<hr class='ipsHr'>
	<br>

	<section>
		<a id='description'></a>
		<h2>Description</h2>

		<p>The Lightbox widget implements a commonly-seen interface convention whereby images can be clicked to view them at a larger size inside a popup that appears on the page. In addition, related images are grouped so that they can be scrolled through inside the popup. IPS4's lightbox also supports built-in commenting.</p>
	</section>

	<section>
		<a id='example'></a>
		<h2>Example</h2>

		<div class='ipsAreaBackground ipsPad cExample'>
			<ul class='ipsList_inline'>
				<li>
					<a href='http://i.imgur.com/Kp2hE0H.jpg' data-ipsLightbox data-ipsLightbox-group='test'><img src='http://i.imgur.com/Kp2hE0Hs.jpg' class='ipsImage_thumb'></a>
				</li>
				<li>
					<a href='http://i.imgur.com/74x2uJC.jpg' data-ipsLightbox data-ipsLightbox-group='test'><img src='http://i.imgur.com/74x2uJCs.jpg' class='ipsImage_thumb'></a>
				</li>
				<li>
					<a href='http://i.imgur.com/h8EKqBE.jpg' data-ipsLightbox data-ipsLightbox-group='test'><img src='http://i.imgur.com/h8EKqBEs.jpg' class='ipsImage_thumb'></a>
				</li>
				<li>
					<a href='http://i.imgur.com/j17kJXT.jpg' data-ipsLightbox data-ipsLightbox-group='test'><img src='http://i.imgur.com/j17kJXTs.jpg' class='ipsImage_thumb'></a>
				</li>
				<li>
					<a href='http://i.imgur.com/UW7VTy6.jpg' data-ipsLightbox data-ipsLightbox-group='test'><img src='http://i.imgur.com/UW7VTy6s.jpg' class='ipsImage_thumb'></a>
				</li>
			</ul>
		</div>
	</section>

	<section>
		<a id='usage'></a>
		<h2>Usage</h2>

		<p>A lightbox can be initialized on a single image as simply as:</p>

<pre class='prettyprint'>
&lt;img src='http://url/to/image' <strong>data-ipsLightbox</strong>&gt;
</pre>

		<p>However, it's likely additional functionality would be desired. A common practice is to link to a large image but display a small thumb image on the page. In this situation, the lightbox is initialized on the link:</p>

<pre class='prettyprint'>
&lt;a href='http://url/to/image' <strong>data-ipsLightbox</strong>&gt;&lt;img src='http://url/to/thumb'&gt;&lt;/a&gt;
</pre>

		<p>In both of the above example, a single image will be displayed in the lightbox. It's also possible to group images together - such as in a photo album, or a group of images for a single content item. In this case, you should initialize a lightbox on <em>all</em> of the items, and include the <em>group</em> option which should be consistent between them:</p>

<pre class='prettyprint'>
&lt;a href='http://url/to/image1' data-ipsLightbox <strong>data-ipsLightbox-group='example'</strong>&gt;&lt;img src='http://url/to/thumb1'&gt;&lt;/a&gt;
&lt;a href='http://url/to/image2' data-ipsLightbox <strong>data-ipsLightbox-group='example'</strong>&gt;&lt;img src='http://url/to/thumb2'&gt;&lt;/a&gt;
&lt;a href='http://url/to/image3' data-ipsLightbox <strong>data-ipsLightbox-group='example'</strong>&gt;&lt;img src='http://url/to/thumb3'&gt;&lt;/a&gt;
</pre>

	</section>

	<section>
		<a id='options'></a>
		<h2>Options</h2>

		<ul class='ipsDataList'>
			<li class='ipsDataItem'>
				<div class='ipsDataItem_main'>
					<h4 class='ipsDataItem_title'><code><strong>group</strong></code></h4> 
					<span class='ipsType_light'>
						<em><strong>String</strong></em>; <em>optional</em>
					</span>
					<br><br>
					<div class='ipsDataItem_meta'>
						If provided, the widget will display other images from the same group in the lightbox.
					</div>
				</div>
			</li>
			<li class='ipsDataItem'>
				<div class='ipsDataItem_main'>
					<h4 class='ipsDataItem_title'><code><strong>commentsURL</strong></code></h4> 
					<span class='ipsType_light'>
						<em><strong>String</strong></em>; <em>optional</em>
					</span>
					<br><br>
					<div class='ipsDataItem_meta'>
						If provided, a comments panel will be shown beside this image when it is active in the lightbox. The comments will be loaded from the URL provided here.
					</div>
				</div>
			</li>
			<li class='ipsDataItem'>
				<div class='ipsDataItem_main'>
					<h4 class='ipsDataItem_title'><code><strong>fullURL</strong></code></h4> 
					<span class='ipsType_light'>
						<em><strong>String</strong></em>; <em>optional</em>
					</span>
					<br><br>
					<div class='ipsDataItem_meta'>
						By default, the full size image will be loaded from the <em>href</em> attribute on the <code>a</code> tag. If this option is provided, the full size image will instead be loaded from this URL.
					</div>
				</div>
			</li>
			<li class='ipsDataItem'>
				<div class='ipsDataItem_main'>
					<h4 class='ipsDataItem_title'><code><strong>className</strong></code></h4> 
					<span class='ipsType_light'>
						<em><strong>String</strong></em>; <em>optional</em>; default value: <em>ipsLightbox</em>
					</span>
					<br><br>
					<div class='ipsDataItem_meta'>
						Allows you to provide a classname prefix which will be used when the lightbox UI is built, enabling a custom UI to be shown.
					</div>
				</div>
			</li>
			<li class='ipsDataItem ipsFaded'>
				<div class='ipsDataItem_main'>
					<h4 class='ipsDataItem_title'><code><strong>preload</strong></code> <span class='ipsBadge ipsBadge_style3'>Not yet supported</span></h4> 
					<span class='ipsType_light'>
						<em><strong>Boolean</strong></em>; <em>optional</em>; default value: <em>true</em>
					</span>
					<br><br>
					<div class='ipsDataItem_meta'>
						Determines whether images in the lightbox should be preloaded when the lightbox is opened, designed to reduce wait time for the user as they scroll through images.
					</div>
				</div>
			</li>
		</ul>
	</section>

	<section>
		<a id='events'></a>
		<h2>Events</h2>

		<p>Events are emitted on the image that <em>originally</em> opened the lightbox - regardless of whether the user has navigated to a different image in the group.</p>

		<ul class='ipsDataList'>
			<li class='ipsDataItem'>
				<div class='ipsDataItem_main'>
					<h4 class='ipsDataItem_title'><code><strong>lightboxImageShown</strong></code></h4> 
					<ul class='ipsDataItem_subList'>
						<li class='ipsType_light'>
							<code>image</code>
							<ul class='ipsDataItem_subList'>
								<li class='ipsType_light'><code>commentsURL</code> - The URL, if any, for this image's comments</li>
								<li class='ipsType_light'><code>elem</code> - Reference to this image's widget element (may different from <em>imageElem</em> if a link launches the lightbox)</li>
								<li class='ipsType_light'><code>imageElem</code> - Reference to the actual image element</li>
								<li class='ipsType_light'><code>largeImage</code> - URL to the fullsize image</li>
								<li class='ipsType_light'><code>originalImage</code> - URL to the image that already existed on the page (e.g. a thumbnail)</li>
								<li class='ipsType_light'><code>meta</code> - The meta information for the image, if any</li>
							</ul>
						</li>
						<li class='ipsType_light'><code>triggerElem</code> - Reference to the widget element that <em>originally</em> opened the lightbox, which may or may not be the image being viewed.</li>
					</ul>
					<br>
					<div class='ipsDataItem_meta'>
						Triggered when the lightbox shows a new image.
					</div>
				</div>
			</li>
			<li class='ipsDataItem'>
				<div class='ipsDataItem_main'>
					<h4 class='ipsDataItem_title'><code><strong>lightboxCommentsLoaded</strong></code></h4> 
					<ul class='ipsDataItem_subList'>
						<li class='ipsType_light'>
							<code>image</code>
							<ul class='ipsDataItem_subList'>
								<li class='ipsType_light'><code>commentsURL</code> - The URL, if any, for this image's comments</li>
								<li class='ipsType_light'><code>elem</code> - Reference to this image's widget element (may different from <em>imageElem</em> if a link launches the lightbox)</li>
								<li class='ipsType_light'><code>imageElem</code> - Reference to the actual image element</li>
								<li class='ipsType_light'><code>largeImage</code> - URL to the fullsize image</li>
								<li class='ipsType_light'><code>originalImage</code> - URL to the image that already existed on the page (e.g. a thumbnail)</li>
								<li class='ipsType_light'><code>meta</code> - The meta information for the image, if any</li>
							</ul>
						</li>
						<li class='ipsType_light'><code>triggerElem</code> - Reference to the widget element that <em>originally</em> opened the lightbox, which may or may not be the image being viewed.</li>
						<li class='ipsType_light'><code>commentsArea</code> - Reference to the comments panel inside the lightbox.</li>
					</ul>
					<br>
					<div class='ipsDataItem_meta'>
						Triggered when the lightbox has finished loading the comments from the <em>commentsURL</em>.
					</div>
				</div>
			</li>
		</ul>
	</section>
</article>