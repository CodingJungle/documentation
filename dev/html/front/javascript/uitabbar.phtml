<ips:template parameters="" />

<div class='ipsPageHeader ipsAreaBackground ipsPad ipsClearfix ipsSpacer_bottom'>
	<h1 class='ipsType_pageTitle'>TabBar Widget</h1>
	<p class='ipsPageHeader_info'>
		A widget for displaying a tab bar which can auto-load content as needed
	</p>
</div>
<br>

<article class='ipsType_normal ipsType_richText'>

	<strong>In this section:</strong>
	<ul class='ipsList_inline'>
		<li><a href='#description'>Description</a></li>
		<li><a href='#usage'>Usage</a></li>
		<li><a href='#options'>Options</a></li>
		<li><a href='#events'>Events</a></li>
	</ul>
	<hr class='ipsHr'>
	<br>

	<section>
		<a id='description'></a>
		<h2>Description</h2>
		<p>The tab bar widget allows users to switch between individual panels of content. The widget can build panels either from content that already exists on the page, or by loading it from a remote source. The browser URL can additionally be updated to reflect the permalink of the individual tab being selected.</p>
	</section>

	<section>
		<a id='usage'></a>
		<h2>Usage</h2>

		<p>The tab bar itself should be constructed manually in the markup, with the tabs linking to a static page. This enables the page to be used even when javascript is enabled. This is the basic markup for a tab bar:</p>

<pre class='prettyprint'>
&lt;!-- The tab bar --&gt;
&lt;div class='ipsTabs ipsClearfix' id='elTabBar' <strong>data-ipsTabBar data-ipsTabBar-contentArea='#elTabContent'</strong>&gt;
	&lt;a href='#elTabBar' data-action='expandTabs'&gt;&lt;i class='icon-caret-down'&gt;&lt;/i&gt;&lt;/a&gt;
	&lt;ul role='tablist'&gt;
		&lt;li role='presentation'&gt;
			&lt;a href='...' role='tab' id='elTabOne' class='ipsTabs_item' aria-selected="true"&gt;
				Tab One
			&lt;/a&gt;
		&lt;/li&gt;
		&lt;li role='presentation'&gt;
			&lt;a href='...' id='elTabTwo' role='tab' class='ipsTabs_item'&gt;
				Tab Two
			&lt;/a&gt;
		&lt;/li&gt;
	&lt;/ul&gt;
&lt;/div&gt;

&lt;!-- The tab panel wrapper --&gt;
&lt;section id='elTabContent'&gt;&lt;/section&gt;
</pre>

		<p>The key details here are:</p>

		<ul class='ipsList_bullets'>
			<li>The widget key attributes belong on the tab bar wrapper element.</li>
			<li>The first child of the tab bar should be an element with the attribute <code>data-action='expandTabs'</code>. This is used when the tab bar is displayed on a small device. For accessibility, it should be an anchor link to the tab bar element.</li>
			<li>Each tab links to a static page - this is the same URL that is called to fetch content remotely, too.</li>
			<li>A panel wrapper is present, which will receive the panels when the tab widget creates them.</li>
			<li>This example is also marked up with ARIA properties for accessibility, such as <code>role='tab'</code> and <code>aria-selected='true'</code>.</li>
		</ul>

		<p>If content exists inside the panel wrapper element at the time the tab widget is initialized, the widget will take this content, create a panel from it, and treat it as the content for the default selected tab. This mechanism means developers can include the default content into the page very easily, without it needing to be fetched with another AJAX request.</p>

		<p>Panels can be created manually, rather than having them load via AJAX. To do so, a panel element should exist inside the panel wrapper element and have the correct ID. The ID takes this format:</p>

<pre class='prettyprint'>
&lt;panelPrefix&gt;_&lt;barID&gt;_&lt;tabID&gt;_panel
</pre>

		<p>Where the values are:</p>

		<ul class='ipsList_bullets ipsType_normal'>
			<li><code>panelPrefix</code> is the prefix set in the options (see below); <code>ipsTabs</code> by default</li>
			<li><code>barID</code> is the ID set on the tab bar element</li>
			<li><code>tabID</code> is the ID set on the individual tab element that this panel relates to</li>
		</ul>
	</section>

	<section>
		<a id='options'></a>
		<h2>Options</h2>

		<ul class='ipsDataList'>
			<li class='ipsDataItem'>
				<div class='ipsDataItem_main'>
					<h4 class='ipsDataItem_title'><code><strong>contentArea</strong></code></h4> 
					<span class='ipsType_light'>
						<em><strong>Selector</strong></em>; <em><strong>required</strong></em>
					</span>
					<br><br>
					<div class='ipsDataItem_meta'>
						A CSS selector which will locate the panel container element into which panels will be created. For example, <code>#elTabContent</code>.
					</div>
				</div>
			</li>
			<li class='ipsDataItem'>
				<div class='ipsDataItem_main'>
					<h4 class='ipsDataItem_title'><code><strong>updateURL</strong></code></h4> 
					<span class='ipsType_light'>
						<em><strong>Boolean</strong></em>; <em>optional</em>; default value: <em>true</em>
					</span>
					<br><br>
					<div class='ipsDataItem_meta'>
						Determines whether the browser address bar will be updated with the URL to individual tabs when the user selects them. This allows individual tabs to be navigated to and appear in the browser's history.
					</div>
				</div>
			</li>
			<li class='ipsDataItem'>
				<div class='ipsDataItem_main'>
					<h4 class='ipsDataItem_title'><code><strong>itemSelector</strong></code></h4> 
					<span class='ipsType_light'>
						<em><strong>Selector</strong></em>; <em>optional</em>; default value: <em>.ipsTab_item</em>
					</span>
					<br><br>
					<div class='ipsDataItem_meta'>
						A CSS selector which will locate the tab elements within the tab bar, e.g. <code>.ipsTab_item</code>.
					</div>
				</div>
			</li>
			<li class='ipsDataItem'>
				<div class='ipsDataItem_main'>
					<h4 class='ipsDataItem_title'><code><strong>activeClass</strong></code></h4> 
					<span class='ipsType_light'>
						<em><strong>String</strong></em>; <em>optional</em>; default value: <em>ipsTabs_activeItem</em>
					</span>
					<br><br>
					<div class='ipsDataItem_meta'>
						A classname which will be applied to the currently-active tab element.
					</div>
				</div>
			</li>
			<li class='ipsDataItem'>
				<div class='ipsDataItem_main'>
					<h4 class='ipsDataItem_title'><code><strong>loadingClass</strong></code></h4> 
					<span class='ipsType_light'>
						<em><strong>String</strong></em>; <em>optional</em>; default value: <em>ipsLoading ipsTabs_loadingContent</em>
					</span>
					<br><br>
					<div class='ipsDataItem_meta'>
						A classname which will be applied to the panel container element when new tab content is being loaded.
					</div>
				</div>
			</li>
			<li class='ipsDataItem'>
				<div class='ipsDataItem_main'>
					<h4 class='ipsDataItem_title'><code><strong>panelClass</strong></code></h4> 
					<span class='ipsType_light'>
						<em><strong>String</strong></em>; <em>optional</em>; default value: <em>ipsTabs_panel</em>
					</span>
					<br><br>
					<div class='ipsDataItem_meta'>
						A classname which will be applied to individual panels as they are created.
					</div>
				</div>
			</li>
			<li class='ipsDataItem'>
				<div class='ipsDataItem_main'>
					<h4 class='ipsDataItem_title'><code><strong>panelPrefix</strong></code></h4> 
					<span class='ipsType_light'>
						<em><strong>String</strong></em>; <em>optional</em>; default value: <em>ipsTabs</em>
					</span>
					<br><br>
					<div class='ipsDataItem_meta'>
						A prefix which will be used when generating a panel ID. This helps to prevent potential naming conflicts with other elements. The default value should be fine in most situations.
					</div>
				</div>
			</li>
			<li class='ipsDataItem'>
				<div class='ipsDataItem_main'>
					<h4 class='ipsDataItem_title'><code><strong>defaultTab</strong></code></h4> 
					<span class='ipsType_light'>
						<em><strong>Selector</strong></em>; <em>optional</em>
					</span>
					<br><br>
					<div class='ipsDataItem_meta'>
						If provided, this option is used to set the default tab when the tab bar first loads. If not provided, the first tab with the class specified by <code>activeClass</code> is looked for; finally, if all else fails, the first tab in the bar is selected.
					</div>
				</div>
			</li>
			<li class='ipsDataItem'>
				<div class='ipsDataItem_main'>
					<h4 class='ipsDataItem_title'><code><strong>stretch</strong></code></h4> 
					<span class='ipsType_light'>
						<em><strong>Selector</strong></em>; <em>optional</em>; default value: <em>false</em>
					</span>
					<br><br>
					<div class='ipsDataItem_meta'>
						If <em>true</em>, the tabs in the bar will share the available space equally between themselves, thus taking up the full width of the bar.
					</div>
				</div>
			</li>
		</ul>
	</section>

	<section>
		<a id='events'></a>
		<h2>Events</h2>

		<p>Events are emitted on the tab bar wrapper.</p>

		<ul class='ipsDataList'>
			<li class='ipsDataItem'>
				<div class='ipsDataItem_main'>
					<h4 class='ipsDataItem_title'><code><strong>tabChanged</strong></code></h4> 
					<ul class='ipsDataItem_subList'>
						<li class='ipsType_light'><code>barID</code> - ID of the tab bar wrapper</li>
						<li class='ipsType_light'><code>tabID</code> - ID of the selected tab</li>
						<li class='ipsType_light'><code>tab</code> - Reference to the newly-selected tab element</li>
					</ul>
					<br>
					<div class='ipsDataItem_meta'>
						Triggered when the selected tab changes.
					</div>
				</div>
			</li>
		</ul>
	</section>
</article>