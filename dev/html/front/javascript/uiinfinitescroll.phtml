<ips:template parameters="" />

<div class='ipsPageHeader ipsAreaBackground ipsPad ipsClearfix ipsSpacer_bottom'>
	<h1 class='ipsType_pageTitle'>InfiniteScroll Widget</h1>
	<p class='ipsPageHeader_info'>
		A widget that implements infinite scroll behavior
	</p>
</div>
<br>

<article class='ipsType_normal ipsType_richText'>

	<strong>In this section:</strong>
	<ul class='ipsList_inline'>
		<li><a href='#description'>Description</a></li>
		<li><a href='#usage'>Usage</a></li>
		<li><a href='#options'>Options</a></li>
		<li><a href='#events'>Events</a></li>
	</ul>
	<hr class='ipsHr'>
	<br>

	<section>
		<a id='description'></a>
		<h2>Description</h2>

		<p>Infinite scrolling is a technique whereby new content is loaded into the page automatically when the user scrolls near the bottom of the page or section. It's a technique commonly associated with sites like Facebook and Twitter.</p>

		<p class='cNote'>Infinite scrolling can present user interface challenges if not used widely, including not being able to reach links at the bottom of the page. Before choosing to use infinite scrolling, consider the usability impacts it may have. It may be more appropriate to display a button at the bottom of your content inviting a user to load more content manually.</p>

		<p>In order to progressively enhance the page, and ensure users with javascript disabled can still access content, infinite scrolling in IPS4 works with the standard pagination markup.</p>
	</section>

	<section>
		<a id='usage'></a>
		<h2>Usage</h2>

		<p>The infinite scroll widget works by having the content inside of a <em>scroll scope</em> element. The element has a fixed height, and hides its overflow so that it scrolls. The content is placed inside a <em>container</em> element and is an arbitrary length, and as the user scrolls and the end of the container approaches the bottom of the scroll scope, the widget will load more content to display.</p>

		<div style='background: rgba(255,0,0,0.6); width: 150px; height: 200px; padding: 5px;'>
			Scroll Scope
			<div style='background: rgba(0,255,0,0.6); width: 140px; height: 215px; margin-top: 5px; padding: 5px;'>
				Container
			</div>
		</div>
		<br><br>

		<p>A standard pagination control should exist inside the scroll scope element; it can be hidden from javascript-enabled browsers by placing it inside a <code>noscript</code> tag.</p>
	</section>

	<section>
		<a id='options'></a>
		<h2>Options</h2>

		<ul class='ipsDataList'>
			<li class='ipsDataItem'>
				<div class='ipsDataItem_main'>
					<h4 class='ipsDataItem_title'><code><strong>scrollScope</strong></code></h4> 
					<span class='ipsType_light'>
						<em><strong>Selector</strong></em>; <em>optional</em>; default value: <em>body</em>
					</span>
					<br><br>
					<div class='ipsDataItem_meta'>
						A selector that idenfities the element which will be the scroll scope; that is, the element that has a fixed height and which the user will scroll to show more content.
					</div>
				</div>
			</li>
			<li class='ipsDataItem'>
				<div class='ipsDataItem_main'>
					<h4 class='ipsDataItem_title'><code><strong>container</strong></code></h4> 
					<span class='ipsType_light'>
						<em><strong>Selector</strong></em>; <em>required</em>
					</span>
					<br><br>
					<div class='ipsDataItem_meta'>
						A selector identifying the container element inside which your content should exist.
					</div>
				</div>
			</li>
			<li class='ipsDataItem'>
				<div class='ipsDataItem_main'>
					<h4 class='ipsDataItem_title'><code><strong>distance</strong></code></h4> 
					<span class='ipsType_light'>
						<em><strong>Mixed</strong></em>; <em>optional</em>; default value: <em>50</em>
					</span>
					<br><br>
					<div class='ipsDataItem_meta'>
						A value that determines how close to the bottom of the <em>scrollScope</em> the <em>container</em> must be to trigger the loading of new content. The value can either be a number (e.g. <code>50</code>) or a percentage (e.g. <code>10%</code>) which represents a percentage height of the <em>scrollScope</em> element.
					</div>
				</div>
			</li>
			<li class='ipsDataItem'>
				<div class='ipsDataItem_main'>
					<h4 class='ipsDataItem_title'><code><strong>url</strong></code></h4> 
					<span class='ipsType_light'>
						<em><strong>String</strong></em>; <em>required</em>
					</span>
					<br><br>
					<div class='ipsDataItem_meta'>
						The URL that is called to retrieve more content from the server.
					</div>
				</div>
			</li>
			<li class='ipsDataItem'>
				<div class='ipsDataItem_main'>
					<h4 class='ipsDataItem_title'><code><strong>pageParam</strong></code></h4> 
					<span class='ipsType_light'>
						<em><strong>String</strong></em>; <em>optional</em>; default value: <em>page</em>
					</span>
					<br><br>
					<div class='ipsDataItem_meta'>
						The parameter used to pass the page value when retrieving more content from the server.
					</div>
				</div>
			</li>
			<li class='ipsDataItem'>
				<div class='ipsDataItem_main'>
					<h4 class='ipsDataItem_title'><code><strong>loadingTpl</strong></code></h4> 
					<span class='ipsType_light'>
						<em><strong>Template key</strong></em>; <em>optional</em>; default value: <em>core.infScroll.loading</em>
					</span>
					<br><br>
					<div class='ipsDataItem_meta'>
						The template used to render the 'loading' text when the widget is currently loading new content.
					</div>
				</div>
			</li>
			<li class='ipsDataItem'>
				<div class='ipsDataItem_main'>
					<h4 class='ipsDataItem_title'><code><strong>pageBreakTpl</strong></code></h4> 
					<span class='ipsType_light'>
						<em><strong>Mixed</strong></em>; <em>optional</em>; default value: <em>core.infScroll.pageBreak</em>
					</span>
					<br><br>
					<div class='ipsDataItem_meta'>
						The template used to render page breaks when content is loaded into the widget. This assists the user in following which page of results they are on. If a template key, that template will be used to build the break; if <code>false</code>, no page break will be inserted.
					</div>
				</div>
			</li>
		</ul>
	</section>

	<section>
		<a id='events'></a>
		<h2>Events</h2>

		<p>Events are emitted on the main widget element.</p>

		<ul class='ipsDataList'>
			<li class='ipsDataItem'>
				<div class='ipsDataItem_main'>
					<h4 class='ipsDataItem_title'><code><strong>infScrollPageLoaded</strong></code></h4> 
					<ul class='ipsDataItem_subList'>
						<li class='ipsType_light'><code>page</code> - The page number that was loaded</li>
					</ul>
					<br>
					<div class='ipsDataItem_meta'>
						Triggered when the widget loads and inserts new content into the <em>container</em>.
					</div>
				</div>
			</li>
		</ul>
	</section>
</article>