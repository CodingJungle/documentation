<ips:template parameters="" />

<div class='ipsPageHeader ipsAreaBackground ipsPad ipsClearfix ipsSpacer_bottom'>
	<h1 class='ipsType_pageTitle'>Position Utility (<code>ips.utils.position</code>)</h1>
	<p class='ipsPageHeader_info'>
		Utility for dealing with element positioning
	</p>
</div>
<br>

<article class='ipsType_normal ipsType_richText'>

	<strong>In this section:</strong>
	<ul class='ipsList_inline'>
		<li><a href='#description'>Description</a></li>
		<li><a href='#example'>Example</a></li>
		<li><a href='#methods'>Methods</a></li>
	</ul>
	<hr class='ipsHr'>
	<br>

	<section>
		<a id='description'></a>
		<h2>Description</h2>

		<p>Dealing with element positioning - getting current positions, working out new positions, and so on - is a common task in a web application such as IPS4. This utility aims to make that easier by providing some methods to do the calculations for you.</p>
	</section>

	<section>
		<a id='methods'></a>
		<h2>Methods</h2>

		<ul class='ipsDataList'>
			<li class='ipsDataItem'>
				<div class='ipsDataItem_main'>
					<pre class='prettyprint'><code><em>object</em> <strong>getElemPosition</strong>( <em>mixed</em> elem )</code></pre> 
					<ul class='ipsDataItem_subList'>
						<li class='ipsType_light'><code>elem</code> - DOM element or jQuery object containing the element to work with</li>
					</ul>
					<br>
					<div class='ipsDataItem_meta'>
						Returns various positioning information for the provided element.
					</div>
					<br>
					<strong>Returned object</strong>
					<ul class='ipsDataItem_subList'>
						<li class='ipsType_light'>
							<code>absPos</code>
							<ul class='ipsDataItem_subList'>
								<li class='ipsType_light'><code>left</code> - Absolute left position</li>
								<li class='ipsType_light'><code>right</code> - Absolute right position (i.e. left + width)</li>
								<li class='ipsType_light'><code>top</code> - Absolute top position</li>
								<li class='ipsType_light'><code>bottom</code> - Absolute bottom position (i.e. top + height)</li>
							</ul>
						</li>
						<li class='ipsType_light'>
							<code>viewportOffset</code>
							<ul class='ipsDataItem_subList'>
								<li class='ipsType_light'><code>left</code> - Viewport left offset (i.e. left offset - left scroll postion)</li>
								<li class='ipsType_light'><code>top</code> - Viewport top offset (i.e. top offset - top scroll position)</li>
							</ul>
						</li>
						<li class='ipsType_light'>
							<code>offsetParent</code> - Reference to the element's <em>offsetParent</em>. That is, the closest element that has a non-static position.
						</li>
						<li class='ipsType_light'>
							<code>fixed</code> - <em>true</em> if the element has any ancestors which have fixed positioning or have one of the <em>table</em> display values.
						</li>
					</ul>
				</div>
			</li>
			<li class='ipsDataItem'>
				<div class='ipsDataItem_main'>
					<pre class='prettyprint'><code><em>object</em> <strong>positionElem</strong>( <em>object</em> options )</code></pre> 
					<ul class='ipsDataItem_subList'>
						<li class='ipsType_light'>
							<code>options</code>
							<ul class='ipsDataItem_subList'>
								<li class='ipsType_light'><code>trigger</code> - The trigger element for this popup; that is, the element that the popup should be 'attached' to</li>
								<li class='ipsType_light'><code>target</code> - The element to be positioned</li>
								<li class='ipsType_light'><code>targetContainer</code> - The element that contains the element to be positioned. Assumed to be <code>body</code> if not provided.</li>
								<li class='ipsType_light'><code>center</code> - (Boolean) Specifies whether the method should attempt to position the <em>target</em> centered relative to the <em>trigger</em></li>
								<li class='ipsType_light'><code>above</code> - (Boolean) Specifies whether the method should attempt to position the <em>target</em> above the <em>trigger</em>. By default, a best fit is attempted.</li>
							</ul>
						</li>
					</ul>
					<br>
					<div class='ipsDataItem_meta'>
						Returns positioning values, allowing the calling method to appropriate position the element with a best-fit on the screen. This method does not apply the positioning values to the element itself.
					</div>
					<br>
					<strong>Returned object</strong>
					<ul class='ipsDataItem_subList'>
						<li class='ipsType_light'>
							<code>top</code> - Ideal <em>top</em> position, in pixels
						</li>
						<li class='ipsType_light'>
							<code>left</code> - Ideal <em>left</em> position, in pixels
						</li>
						<li class='ipsType_light'>
							<code>fixed</code> - Whether the element should have fixed positioning applied (because an ancestor does)
						</li>
						<li class='ipsType_light'>
							<code>location</code>
							<ul class='ipsDataItem_subList'>
								<li class='ipsType_light'><code>horizontal</code> - Best horizontal location for the element; either <code>center</code>, <code>left</code> or <code>right</code></li>
								<li class='ipsType_light'><code>vertical</code> - Best vertical location for the element; either <code>bottom</code> or <code>top</code></li>
							</ul>
						</li>
					</ul>
				</div>
			</li>
			<li class='ipsDataItem'>
				<div class='ipsDataItem_main'>
					<pre class='prettyprint'><code><em>boolean</em> <strong>hasFixedParents</strong>( <em>mixed</em> elem )</code></pre> 
					<ul class='ipsDataItem_subList'>
						<li class='ipsType_light'>
							<code>elem</code> - The element whose ancestors are being checked for fixed positioning. Either a DOM node or a jQuery object.
						</li>
					</ul>
					<br>
					<div class='ipsDataItem_meta'>
						Returns <em>true</em> if any of the element's descendents have <em>fixed</em> positioning or have one of the table display classes (e.g. <em>table-row</em>).
					</div>
				</div>
			</li>
			<li class='ipsDataItem'>
				<div class='ipsDataItem_main'>
					<pre class='prettyprint'><code><em>object</em> <strong>getElemDims</strong>( <em>mixed</em> elem )</code></pre> 
					<ul class='ipsDataItem_subList'>
						<li class='ipsType_light'>
							<code>elem</code> - The element whose dimensions are being returned. Either a DOM node or a jQuery object.
						</li>
					</ul>
					<br>
					<div class='ipsDataItem_meta'>
						Returns current dimension information for the element.
					</div>
					<br>
					<strong>Returned object</strong>
					<ul class='ipsDataItem_subList'>
						<li class='ipsType_light'>
							<code>width</code> - Current width in pixels
						</li>
						<li class='ipsType_light'>
							<code>height</code> - Current height in pixels
						</li>
						<li class='ipsType_light'>
							<code>outerWidth</code> - Current outer width of the element; that is, the width plus border and padding
						</li>
						<li class='ipsType_light'>
							<code>outerHeight</code> - Current outer height of the element; that is, the width plus border and padding
						</li>
					</ul>
				</div>
			</li>
		</ul>
	</section>
</article>