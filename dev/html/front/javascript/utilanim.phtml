<ips:template parameters="" />

<div class='ipsPageHeader ipsAreaBackground ipsPad ipsClearfix ipsSpacer_bottom'>
	<h1 class='ipsType_pageTitle'>Animation Utility (<code>ips.utils.anim</code>)</h1>
	<p class='ipsPageHeader_info'>
		Utility for animating elements with CSS
	</p>
</div>
<br>

<article class='ipsType_normal ipsType_richText'>

	<strong>In this section:</strong>
	<ul class='ipsList_inline'>
		<li><a href='#description'>Description</a></li>
		<li><a href='#example'>Example</a></li>
		<li><a href='#anims'>Default Animations</a></li>
		<li><a href='#methods'>Methods</a></li>
	</ul>
	<hr class='ipsHr'>
	<br>

	<section>
		<a id='description'></a>
		<h2>Description</h2>

		<p>The animation utility provides a number of methods to help animate elements. While jQuery supports simple animation, these are handled by javascript, and can therefore be slow on mobile devices. By leveraging CSS animation instead, many mobile devices can even offer hardware-accelerated animations, resulting in smooth transistions.</p>

		<p>The utility allows you to run animations on elements, and also create new animations to be run on them.</p>

		<p>If the user's browser does not support CSS animations, a fallback animation (using jQuery) will be used.</p>
	</section>

	<section>
		<a id='example'></a>
		<h2>Example</h2>

<pre class='prettyprint'>
ips.utils.anim.go( 'fadeOut', $( element ) );
ips.utils.anim.go( 'pulse fast', $( element ) );
</pre>
	</section>

	<section>
		<a id='anims'></a>
		<h2>Default Animations</h2>

		<p>In the examples below, the button text is the animation key that is used in the <code>go</code> method.</p>

		<div class='ipsAreaBackground ipsPad' style='height: 80px'>
			<div style='height: 50px; background: #7c5785; color: #fff' class='ipsPad' id='elExampleElem'>Example element</div>
		</div>
		<br><br>

		<div class='ipsGrid' id='elAnimationGrid'>
			<div class='ipsGrid_span4'>
				<a href='#' id='animFadeIn' class='ipsButton ipsButton_primary ipsButton_fullWidth'>fadeIn</a>
			</div>
			<div class='ipsGrid_span4'>
				<a href='#' id='animFadeOut' class='ipsButton ipsButton_primary ipsButton_fullWidth'>fadeOut</a>
			</div>
			<div class='ipsGrid_span4'>
				<a href='#' id='animFadeInDown' class='ipsButton ipsButton_primary ipsButton_fullWidth'>fadeInDown</a>
			</div>
			<div class='ipsGrid_span4'>
				<a href='#' id='animFadeOutDown' class='ipsButton ipsButton_primary ipsButton_fullWidth'>fadeOutDown</a>
			</div>
			<div class='ipsGrid_span4'>
				<a href='#' id='animZoomIn' class='ipsButton ipsButton_primary ipsButton_fullWidth'>zoomIn</a>
			</div>
			<div class='ipsGrid_span4'>
				<a href='#' id='animZoomOut' class='ipsButton ipsButton_primary ipsButton_fullWidth'>zoomOut</a>
			</div>
			<div class='ipsGrid_span4'>
				<a href='#' id='animWobble' class='ipsButton ipsButton_primary ipsButton_fullWidth'>wobble</a>
			</div>
			<div class='ipsGrid_span4'>
				<a href='#' id='animJiggle' class='ipsButton ipsButton_primary ipsButton_fullWidth'>jiggle</a>
			</div>
			<div class='ipsGrid_span4'>
				<a href='#' id='animPulseOnce' class='ipsButton ipsButton_primary ipsButton_fullWidth'>pulseOnce</a>
			</div>
		</div>

		<script type='text/javascript'>
			var elem = $('#elExampleElem');
			var animate = function (name) {
				return function (e) {
					e.preventDefault();
					ips.utils.anim.go( name, elem );
				};
			};

			$('#animFadeIn').on( 'click', animate('fadeIn') );
			$('#animFadeOut').on( 'click', animate('fadeOut') );
			$('#animFadeInDown').on( 'click', animate('fadeInDown') );
			$('#animFadeOutDown').on( 'click', animate('fadeOutDown') );
			$('#animSlideLeft').on( 'click', animate('slideLeft') );
			$('#animBlindDown').on( 'click', animate('blindDown') );
			$('#animBlindUp').on( 'click', animate('blindUp') );
			$('#animZoomIn').on( 'click', animate('zoomIn') );
			$('#animZoomOut').on( 'click', animate('zoomOut') );
			$('#animWobble').on( 'click', animate('wobble') );
			$('#animJiggle').on( 'click', animate('jiggle') );
			$('#animPulseOnce').on( 'click', animate('pulseOnce') );
		</script>
	</section>

	<section>
		<a id='functions'></a>
		<h2>Creating animations</h2>

		<p>There's no set way that animations must be implemented; however, at a minimum, an animation should provide a function called when the browser supports CSS animation, and an alternative fallback function called when CSS animations are not supported. The fallback function may just show or hide the element with no animation if necessary.</p>

		<p>A typical animation method might look like this:</p>

<pre class='prettyprint'>
function (elem, speed) {
	cleanClasses( elem );
	return elem							
			.show()
			.addClass( [ 'ipsAnim', 'ipsAnim_fade', 'ipsAnim_in', speed ].join(' ') )
			.animationComplete( function(){
				cleanClasses( elem );
			});
}
</pre>

		<p>In this example, we first clean the element of other animation classes to prevent conflicts. We then apply the base <code>ipsAnim</code> CSS class, as well as some classes specific to this animation and the speed class passed into the function. Using the special <code>animationComplete</code> method, we then again clear animation classes from the element once the animation has finished transitioning.</p>
	</section>

	<section>
		<a id='methods'></a>
		<h2>Methods</h2>

		<ul class='ipsDataList'>
			<li class='ipsDataItem'>
				<div class='ipsDataItem_main'>
					<pre class='prettyprint'><code><em>void</em> <strong>go</strong>( <em>string</em> animationKey, <em>element</em> elem [, ...] )</code></pre> 
					<ul class='ipsDataItem_subList'>
						<li class='ipsType_light'><code>animationKey</code> - The animation key to use. You can optionally specify a speed (either <code>fast</code>, <code>slow</code> or <code>verySlow</code>, e.g. <code>"fadeInDown fast"</code>
						</li>
						<li class='ipsType_light'><code>elem</code> - One or more elements on which the animation will be performed.</li>
					</ul>
					<br>
					<div class='ipsDataItem_meta'>
						Animates an element.
					</div>
				</div>
			</li>
			<li class='ipsDataItem'>
				<div class='ipsDataItem_main'>
					<pre class='prettyprint'><code><em>element</em> <strong>cleanClasses</strong>( <em>element</em> elem )</code></pre> 
					<ul class='ipsDataItem_subList'>
						<li class='ipsType_light'><code>elem</code> - The element to be cleaned of animation classes.</li>
					</ul>
					<br>
					<div class='ipsDataItem_meta'>
						Removes all animation-related CSS classes from the provided element, returning the same element. The built-in animations call this method themselves.
					</div>
				</div>
			</li>
			<li class='ipsDataItem'>
				<div class='ipsDataItem_main'>
					<pre class='prettyprint'><code><em>void</em> <strong>addTransition</strong>( <em>string</em> name, <em>function</em> cssAnimation, <em>function</em> fallback )</code></pre> 
					<ul class='ipsDataItem_subList'>
						<li class='ipsType_light'><code>name</code> - The name to identify this animation.</li>
						<li class='ipsType_light'><code>cssAnimation</code> - A function which is called when CSS animations are supported. The function receives two parameters; a reference to the element to be animated, and a speed class.</li>
						<li class='ipsType_light'><code>fallback</code> - A function which is called when CSS animations are <strong>not</strong> supported. The function receives one parameter - the element to be animated.</li>
					</ul>
					<br>
					<div class='ipsDataItem_meta'>
						A method which allows you to add custom animations to be used later.
					</div>
				</div>
			</li>
			<li class='ipsDataItem'>
				<div class='ipsDataItem_main'>
					<pre class='prettyprint'><code><em>boolean</em> <strong>isTransition</strong>( <em>string</em> name )</code></pre> 
					<ul class='ipsDataItem_subList'>
						<li class='ipsType_light'><code>name</code> - The animation name to check.</li>
					</ul>
					<br>
					<div class='ipsDataItem_meta'>
						Returns <code>true</code> is the animation name provided has already been registered.
					</div>
				</div>
			</li>
		</ul>
	</section>
</article>
