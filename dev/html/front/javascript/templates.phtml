<ips:template parameters="" />

<div class='ipsPageHeader ipsAreaBackground ipsPad ipsClearfix ipsSpacer_bottom'>
	<h1 class='ipsType_pageTitle'>Templates</h1>
	<p class='ipsPageHeader_info'>
		How to use the client-side templating system
	</p>
</div>
<br>

<article class='ipsType_normal ipsType_richText'>

	<strong>In this section:</strong>
	<ul class='ipsList_inline'>
		<li><a href='#overview'>Overview</a></li>
		<li><a href='#syntax'>Syntax</a></li>
		<li><a href='#registering'>Registering a Template</a></li>
		<li><a href='#rendering'>Rendering a Template</a></li>
		<li><a href='#compiling'>Compiling a Template</a></li>
		<li><a href='#replacing'>Automatic Replacements</a></li>
		<li><a href='#language'>Using Language Strings</a></li>
	</ul>
	<hr class='ipsHr'>
	<br>

	<section>
		<a id='overview'></a>
		<h2>Overview</h2>

		<p>Templates enable you to render HTML on the client-side using pre-defined chunks of markup, interpolated with values which are parsed at runtime.</p>
	</section>

	<section>
		<a id='syntax'></a>
		<h2>Syntax</h2>

		<p>Templates use the <a href='https://github.com/janl/mustache.js'>Mustache</a> templating system. Refer to the Mustache site for the full range of options.</p>

		<p>Basic templates are simple HTML strings, with variables interspersed within them. A variable takes the form <code>&#123;&#123;name&#125;&#125;</code> and is automatically escaped.</p>
	</section>

	<section>
		<a id='registering'></a>
		<h2>Registering a template</h2>

		<p>A template is registered like so:</p>

<pre class='prettyprint'>
ips.templates.set('core.section.item', " \
	&lt;div id='&#123;&#123;id&#125;&#125;'&gt;\
		&lt;h2&gt;&#123;&#123;title&#125;&#125;&lt;/h2&gt;\
		&lt;p&gt;&#123;&#123;statusText&#125;&#125;&lt;/p&gt;\
	&lt;/div&gt;\
");
</pre>
	
		<p><code>ips.templates.set</code> is called with two parameters. The first parameter is an identifier for the template. The format of the identifier is not enforced, but it is recommended that it is effectively namespaced to the application and subsection. The second parameter is the template itself, an HTML string with any required variables contained within it.</p>

		<p>Registering a template makes it available for rendering by other modules.</p>

	</section>

	<section>
		<a id='rendering'></a>
		<h2>Rendering a template</h2>

		<p>The following code could be used to render the template defined in the previous example:</p>

<pre class='prettyprint'>
var result = ips.templates.render('core.section.item', {
	id: 'some_id',
	title: 'Item title',
	statusText: 'Status string'
});
</pre>

		<p><code>ips.templates.render</code> is called, and returns a string which is the parsed template, ready to be sent to the browser. Two parameters are passed; the first is the identifier of the template to be registered, while the second is an object containing values which will replace the variables in the template. Values must be a string, whether by type or by containing a <code>toString</code> method.</p>
	</section>

	<section>
		<a id='compiling'></a>
		<h2>Compiling a template</h2>

		<p>Calling <code>ips.templates.render</code> instructs Mustache to build the template into a function, which is then called. It does this every time the render method is called.</p>

		<p>This can be slow if you are building the same template multiple times (e.g. to build a list of items). Instead, you can <em>compile</em> the template, which returns the function form. You can then call this function repeatedly with your template parameters, and this will be much faster than rendering each iteration afresh.</p>

		<p>You first compile the template, storing the returned function:</p>

<pre class='prettyprint'>
var myTemplate = ips.templates.compile('core.section.item');
</pre>

		<p>Then, to use it, call this variable with your replacements object:</p>

<pre class='prettyprint'>
myTemplate({
	id: 'some_id',
	title: 'Item title',
	statusText: 'Status string'
});
</pre>
	</section>

	<section>
		<a id='replacing'></a>
		<h2>Automatic replacements</h2>

		<p>Some common values will automatically be replaced for you without you needing to include them in your replacements object. They are used as normal with the double-bracket syntax.</p>

		<ul class='ipsList_bullets'>
			<li>
				<code>imgURL</code><br>
				The URL to the current image directory.
			</li>
			<li>
				<code>baseURL</code><br>
				The base URL of the suite.
			</li>
		</ul>
	</section>

	<section>
		<a id='language'></a>
		<h2>Using language strings</h2>

		<p>As with all other areas of the suite, words used in templates should be available for internationalization. In templates, this is achieved by a custom <code>lang</code> block using the Mustache syntax:</p>

<pre>
This is my &#123;&#123;#lang&#125;&#125;<strong>string_key</strong>&#123;&#123;/lang&#125;&#125;.
</pre>

		<p>This will replace the block with the full language string identified by the key <code>string_key</code>. The language string should already have been registered (see the <a href='{url="app=documentation&module=documentation&controller=javascript&do=languages"}'>languages</a> document).</p>
	</section>
</article>