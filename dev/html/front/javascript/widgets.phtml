<ips:template parameters="" />

<div class='ipsPageHeader ipsAreaBackground ipsPad ipsClearfix ipsSpacer_bottom'>
	<h1 class='ipsType_pageTitle'>Creating UI Widgets</h1>
	<p class='ipsPageHeader_info'>
		How to build UI widgets within the framework.
	</p>
</div>
<br>

<article class='ipsType_normal ipsType_richText'>

	<strong>In this section:</strong>
	<ul class='ipsList_inline'>
		<li><a href='#what'>What's a Widget?</a></li>
		<li><a href='#boilerplate'>Basic Widget Boilerplate</a></li>
		<li><a href='#registering'>Registering a UI Widget</a></li>
		<li><a href='#events'>Events</a></li>
		<li><a href='#initializing'>Widget Initialization</a></li>
		<li><a href='#example'>Example Widget</a></li>
	</ul>
	<hr class='ipsHr'>
	<br>

	<section>
		<a id='what'></a>
		<h2>What's a widget?</h2>
		<p>UI widgets are modules which:</p>

		<ul class='ipsList_bullets'>
			<li>Are instantiated on individual DOM nodes</li>
			<li>Provide some kind of UI functionality</li>
			<li>Benefit from being reusable</li>
			<li>Come with an automatic data API and jQuery plugin functionality</li>
		</ul>
		
		<p>A UI widget is created as a standard module, under the <code>ips.ui</code> namespace.</p>

	</section>

	<section>
		<a id='boilerplate'></a>
		<h2>Basic widget boilerplate</h2>

<pre class='prettyprint lang-js'>
;( function($, _, undefined){
	"use strict";

	ips.createModule('ips.ui.widgetName', function(){

		var respond = function (elem, options, e) {
		
		};

		// Register this module as a widget to enable the data API and
		// jQuery plugin functionality
		ips.ui.registerWidget( 'widgetName', ips.ui.widgetName );

		return {
			respond: respond
		};
	});
}(jQuery, _));
</pre>

		<p>In the example above, a module is created with the module path <code>ips.ui.widgetName</code>. UI widget modules <strong>must</strong> expose a response method, either the default <code>respond</code> method or by manually specifying a method when the widget is registered (see below). This is the method that is called when the widget is instantiated.</p>
	</section>

	<section>
		<a id='registering'></a>
		<h2>Registering a UI widget</h2>

		<p>A module is registered as a widget by calling the <code>ips.ui.registerWidget</code> method before the module returns its public methods object.</p>

<pre class='prettyprint'>
ips.ui.registerWidget( <em>string</em> widgetKey, <em>object</em> handlerModule [, <em>array</em> acceptedOptions] [, <em>object</em> widgetOptions] [, <em>function</em> callback] );
</pre>
		
		<p>The method takes the following parameters:</p>
		<br>

		<p>
			<strong><code>widgetKey</code></strong> <span class='ipsType_light'>- required</span><br>
			The key that is used to reference this widget throughout the software. The key is prefixed with <code>ips</code> when used, to prevent naming collisions. The key forms both the data API attribute key, and the name of the jQuery plugin that can be used to instantiate the widget. Assuming the widget key is set to <code>widget</code>, the data API attribute key and jQuery plugin would be:
		</p>

<pre class='prettyprint'>
data-ipsWidget			// Data API attribute
$('element').ipsWidget();	// jQuery plugin method 
</pre>
		<br>

		<p>
			<strong><code>handlerModule</code></strong> <span class='ipsType_light'>- required</span><br>
			A reference to the module that will respond when the widget is instantiated. In practice, this should be a self reference to the module that is being defined.
		</p>
		<br>

		<p>
			<strong><code>acceptedOptions</code></strong> <span class='ipsType_light'>- optional</span><br>
			An array of strings defining the options that this widget will accept. When the widget is instantiated, if these options are defined their values will be passed back to the <code>respond</code> method.
		</p>

<pre class='prettyprint'>
['option1', 'option2']		// Options as defined
data-ipsWidget-option1='...' data-ipsWidget-option2='...'	// Options used when widget created on element
</pre>
		<br>

		<p>
			<strong><code>widgetOptions</code></strong> <span class='ipsType_light'>- optional</span><br>
			An object containing options for how this widget will be registered or instantiated. The available options are:
		</p>

		<ul class='ipsList_bullets ipsType_normal'>
			<li>
				<code>lazyLoad</code><span class='ipsType_light'>- Boolean; default value: <em>false</em></span><br>
				By default, widgets are instantiated as soon as they are seen in the DOM. By setting this option to <code>true</code> the widget is not instantiated until the user interacts with the element.
			</li>
			<li>
				<code>lazyEvent</code><span class='ipsType_light'>- String</span><br>
				If the <code>lazyLoad</code> option is enabled, this option defines the event that will be watched for. When the event name set here is triggered, the widget will be initialized.
			</li>
			<li>
				<code>makejQueryPlugin</code><span class='ipsType_light'>- Boolean; default value: <em>true</em></span><br>
				By default, widgets will have an associated jQuery plugin created to allow them to be instantiated programatically. This functionality can be disabled by setting this option to <code>false</code>.
			</li>
		</ul>
		<br>

		<p>
			<strong><code>callback</code></strong> <span class='ipsType_light'>- optional</span><br>
			A widget must have a response method, which is called when it is initialized on an element. By default, a method with the name <code>respond</code> is looked for, but a different response function can be set by passing it as this parameter.
		</p>
	</section>

	<section>
		<a id='events'></a>
		<h2>Events</h2>

		<p>The javascript framework in IPS4 relies heavily on events. Controllers in particular are completely decoupled and cannot directly communicate with one another. Instead, communication happens by triggering and receiving events.</p>
		<p>It is therefore important that your widget emits events when key actions happen. This allows controllers, other UI widgets, even third-party scripts to respond when something happens within your widget. As an example, the <code>ips.ui.menu</code> widget emits a number of events, including <code>menuOpened</code>, <code>menuClosed</code> and <code>menuItemSelected</code>. Each of these events also provides a number of data items relevant to the event, which can be used by event handlers listening for those events.</p>

		<p>It is this event interaction that facilitates dynamic pages within the IPS Social Suite, so it's important your widget also emits relevant events.</p>

		<p>When you emit an event, it is usually most appropriate to emit it on the element the widget is registered on. Emitting an event uses the standard jQuery syntax:</p>

<pre class='prettyprint'>
$( elem ).trigger( 'myWidgetEvent', { color: 'red', size: 'large' } );
</pre>



	</section>

	<section>
		<a id='initializing'></a>
		<h2>Widget initialization</h2>

		<p>A widget is initialized either as soon as it is seen in the DOM, or, if the <code>lazyLoad</code> option is enabled, when the user interacts with the element the widget is created on. In both cases, the initialization process is the same. The internal widget manager will call the response method for the widget (<code>respond</code> by default, or whatever function is passed as the <code>callback</code> option), passing in the following parameters:</p>
		
<pre class='prettyprint'>
function respond( <em>element</em> elem, <em>array</em> options, <em>event</em> e )
</pre>
		
		<ul class='ipsList_bullets ipsType_normal'>
			<li>
				<code>elem</code><br>
				A reference to the element on which the widget has been initialized
			</li>
			<li>
				<code>options</code><br>
				An array of options that have been specified on the element, and which were in the list of expected options when the widget was first registered. Any other options are ignored and won't be passed through.
			</li>
			<li>
				<code>e</code><br>
				If the widget is initialized in response to a user interaction because <code>lazyLoad</code> is enabled, this parameter will contain the original event object. It will be undefined if the widget is being initialized on load.
			</li>
		</ul>
	</section>

	<section>
		<a id='example'></a>
		<h2>Example widget</h2>

		<p>Let's take a trivial example, and assume we're creating a widget which hides an element:</p>

<pre class='prettyprint'>
;( function($, _, undefined){
	"use strict";

	ips.createModule('ips.ui.hideElem', function(){

		var respond = function (elem, options, e) {
			if( options.animate ){
				$( elem ).fadeOut();
			} else {
				$( elem ).hide();
			}

			$( elem ).trigger( 'hiddenElement' );
		};

		ips.ui.registerWidget( 'hideElem', ips.ui.hideElem, [ 'animate' ], { lazyLoad: true, lazyEvent: 'click' );

		return {
			respond: respond
		};
	});
}(jQuery, _));
</pre>

		<p>When we register the widget, we set up an <em>animate</em> option in the <code>acceptedOptions</code> parameter. In the <code>widgetOptions</code> parameter, we set <em>lazyLoad</em> to true, and the <em>lazyEvent</em> to click - meaning our widget won't be initialized until the user clicks on the element.</p>

		<p>In our <code>respond</code> method, we simply check if <code>options.animate</code> is true, fading out if so and hiding if not.</p>

		<p>This example widget could be initialized on an element like so:</p>

<pre class='prettyprint'>
&lt;button data-ipsHideElem data-ipsHideElem-animate='true'&gt;Click to hide me&lt;/button&gt;
</pre>
		
	</section>
</article>