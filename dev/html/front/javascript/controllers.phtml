<ips:template parameters="" />

<div class='ipsPageHeader ipsAreaBackground ipsPad ipsClearfix ipsSpacer_bottom'>
	<h1 class='ipsType_pageTitle'>Controllers</h1>
	<p class='ipsPageHeader_info'>
		How to build and use controllers
	</p>
</div>
<br>

<article class='ipsType_normal ipsType_richText'>

	<strong>In this section:</strong>
	<ul class='ipsList_inline'>
		<li><a href='#scope'>Global Scope &amp; Closure Wrapping</a></li>
		<li><a href='#strict'>Strict Mode</a></li>
		<li><a href='#comments'>Documentation &amp; Commenting</a></li>
		<li><a href='#semicolons'>Semi-colons</a></li>
		<li><a href='#names'>Names</a></li>
		<li><a href='#logging'>Logging</a></li>
		<li><a href='#linelength'>Line Length</a></li>
		<li><a href='#jquery'>jQuery Chaining</a></li>
	</ul>
	<hr class='ipsHr'>
	<br>

	<section>
		<h2>Overview</h2>

		<p>Controllers are special modules that handle specific functionality on specific pages. They are not necessarily reusable in different contexts. A controller is initialized on an individual element, and that element becomes the controller's <em>scope</em>.</p>

		<p>Controllers typically respond to default browser events and events triggered by UI widgets, emit events asking models to load data, and update the page when models trigger events indicating data has been loaded.</p>

		<p>The scope of a controllers functionality is entirely flexible. A controller might be initialized on a very small fragment of the page and perform just one task, or it might be initialized on a main wrapper and handle functionality for most of the page. A controller can be initialized on an element even if it's a descendent of another element with a controller; this is a common pattern whereby the child controller can emit events that the parent controller can respond to.</p>
	</section>

	<section>
		<h2>Creating a controller</h2>

		<p>A controller is registered in a slightly different way to standard modules; instead, an object literal is passed to the method <code>ips.controller.register</code>:</p>

<pre class='prettyprint'>
;( function($, _, undefined){
	"use strict";

	ips.controller.register('type.controllerID', {
		initialize: function () {
			// Controller events are initialized here
		}
	});
}(jQuery, _));
</pre>

		<p>A controller, at the very least, <strong>must</strong> define an <code>initialize</code> method, which is called when the controller is initialized on an element. The <code>initialize</code> method should set up events that are handled by this controller only; if other initialization tasks are necessary, it is recommended that you define a <code>setup</code> method within the controller, and call that at the start or end of the <code>initialize</code> method.</p>

		<p>Within the controller definition, <code>this</code> refers to the controller instance. That means controller methods can be called internally with <code>this.someMethod()</code>.</p>

		<p>A reference to the element that the controller is initialized on (its <em>scope</em>) is available with the <code>scope</code> property:</p>

<pre class='prettyprint'>
// An example which hides the element
this.scope.hide();
</pre>
	</section>

	<section>
		<h2 class='ipsType_sectionHead'>Handling events</h2>

		<p>Event handling is the core purpose of a controller. There are three special methods available for watching and triggering events. Two are named after their jQuery counterparts, but add controller-specific behavior.
		</p>
		<br>

		<p>
			<code class='ipsType_large'><strong>this.on</strong></code><br>
			Used for watching and responding to events
		</p>
<pre class='prettyprint'>
<strong>this.on</strong>( [<em>element</em> elem,] <em>string</em> eventType [, <em>selector</em> delegate], <em>function</em> callback );
</pre>

		<ul class='ipsList_bullets'>
			<li>
				<code>elem</code><br>
				A reference to the element on which the event will be watched for. If not specified, <code>this.scope</code> is used by default.
			</li>
			<li>
				<code>eventType</code><br>
				The event being watched for. This can be a default browser event, like <em>click</em>, or events from widgets and models, like <em>menuItemSelected</em>.
			</li>
			<li>
				<code>delegate</code><br>
				A delegate selector can optionally be specified. See the <a href='http://api.jquery.com/on/#direct-and-delegated-events'>jQuery documentation</a> for more information.
			</li>
			<li>
				<code>callback</code><br>
				A callback function, called when this event is observed. The function specified here is <em>automatically</em> bound to <code>this</code>, so internal methods can be passed simply by referencing them like so:

				<br>
<pre class='prettyprint'>
this.on( 'click', this.internalMethod );
</pre>
			</li>
		</ul>
		<br>

		<p>
			<code class='ipsType_large'><strong>this.trigger</strong></code><br>
			Used to emit events
		</p>
<pre class='prettyprint'>
this.trigger( [<em>element</em> elem,] <em>string</em> eventType [, <em>object</em> data] );
</pre>
		<br>

		<ul class='ipsList_bullets'>
			<li>
				<code>elem</code><br>
				A reference to the element on which the event will be triggered. If not specified, <code>this.scope</code> is used by default.
			</li>
			<li>
				<code>eventType</code><br>
				The event being triggered.
			</li>
			<li>
				<code>data</code><br>
				An object containing data relevant to the event. Note: if an event is being triggered in a controller in response to a model event occuring, it is good practice to <em>extend</em> the current data object rather than simply passing a new one. This allows data to persist through an entire event lifecycle.
			</li>
		</ul>
		<br>

		<p>
			<code class='ipsType_large'><strong>this.triggerOn</strong></code><br>
			Used to emit events directly on sub-controllers from a parent controller.
		</p>
<pre class='prettyprint'>
this.triggerOn( <em>string</em> controllerName, <em>string</em> eventType [, <em>object</em> data] );
</pre>
		<br>

		<ul class='ipsList_bullets'>
			<li>
				<code>controllerName</code><br>
				The controller name to find and trigger the event on. The wildcard character (*) is supported at the end of this parameter. Examples:

<pre class='prettyprint'>
core.front.core.comment
core.front.core.*
core.*
</pre>
			</li>
			<li>
				<code>eventType</code><br>
				The event being triggered.
			</li>
			<li>
				<code>data</code><br>
				An object containing data relevant to the event.
			</li>
		</ul>

	</section>