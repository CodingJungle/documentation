<ips:template parameters="" />

<div class='ipsPageHeader ipsAreaBackground ipsPad ipsClearfix ipsSpacer_bottom'>
	<h1 class='ipsType_pageTitle'>Icons</h1>
	<p class='ipsPageHeader_info'>
		Using icons within IPS4
	</p>
</div>
<br>

<article class='ipsType_normal ipsType_richText'>

	<strong>In this section:</strong>
	<ul class='ipsList_inline'>
		<li><a href='#usage'>Usage</a></li>
		<li><a href='#consistency'>Icon Consistency</a></li>
	</ul>
	<hr class='ipsHr'>
	<br>

	<section>
		<p>We make use of an icon font called FontAwesome. This enables us to display icons that are accessible, that don't require an additional HTTP request, that can be styled with CSS (and inherit styling automatically), and which scale without loss of quality.</p>
	</section>

	<section>
		<a id='usage'></a>
		<h2>Usage</h2>
		<p>An icon can be included within the markup by using the following code:</p>
		
		<pre class='prettyprint'>&lt;i class='fa fa-iconname'&gt;&lt;/i&gt;</pre>
		
		<p>The list of possible icons and their classnames is available at <a href='http://fortawesome.github.io/Font-Awesome/'>http://fortawesome.github.io/Font-Awesome/</a>. Note that these classnames are <em>not</em> prefixed with <code>ips</code> as with other framework classes; they are left with their default names as they appear on the FontAwesome website.</p>

		<p>Icons can be used anywhere that HTML can be used. For example, within text, on buttons, in menus and more.</p>

		<div class='ipsAreaBackground ipsPad cExample'>
			<button class='ipsButton ipsButton_primary ipsButton_medium'><i class='fa fa-star'></i> &nbsp; Icon on a button</button> &nbsp;&nbsp;&nbsp;&nbsp;<a href='#' data-ipsMenu id='elIconMenu'>A caret icon indicates a dropdown <i class='fa fa-caret-down'></i></a>
			<ul id='elIconMenu_menu' class='ipsHide ipsMenu ipsMenu_auto'>
				<li class='ipsMenu_item'><a href='#'><i class='fa fa-check'></i> Yes, I think so</a></li>
				<li class='ipsMenu_item'><a href='#'><i class='fa fa-times'></i> No, I disagree</a></li>
				<li class='ipsMenu_item'><a href='#'><i class='fa fa-caret-down'></i> This is another caret</a></li>
			</ul>
		</div>
	</section>

	<section id='elIconConsistency'>
		<a id='consistency'></a>
		<h2>Icon consistency</h2>

		<p>It is important that icon use remains relatively consistent throughout the suite. This applies to core developers as well as addon developers. If different icons are used for the same purpose (or worse, an icon is used for a purpose <em>different</em> to it's usual purpose), users will become confused by the software.</p>

		<p>To help alleviate this, we have a list of icons that we generally refer to when choosing an icon to represent an action. Check this list to see if your action already has an associated icon, and lean towards using that instead of choosing another.</p>

		<p>The list below organizes actions, with the title being a general concept. The icon names are FontAwesome icons, without the preceding <em>fa-</em>.
		<br><br>

		<div class='ipsGrid'>
			<div class='ipsGrid_span6'>
				<h3 class='ipsType_reset'>Adding</h3>
				<ul class='ipsList_reset ipsType_large'>
					<li>
						<i class='fa fa-plus-circle ipsType_veryLarge'></i>	<span class='ipsType_light'>plus-circle</span>
					</li>
					<li>
						<i class='fa fa-plus ipsType_veryLarge'></i> <span class='ipsType_light'>plus</span>
					</li>
				</ul>
				<hr class='ipsHr'>

				<h3 class='ipsType_reset'>Deleting</h3>
				<ul class='ipsList_reset ipsType_large'>
					<li>
						<i class='fa fa-times-circle ipsType_veryLarge'></i> <span class='ipsType_light'>times-circle</span>
					</li>
					<li>
						<i class='fa fa-trash-o ipsType_veryLarge'></i> <span class='ipsType_light'>trash-o</span>
					</li>
				</ul>
				<hr class='ipsHr'>

				<h3 class='ipsType_reset'>Editing</h3>
				<ul class='ipsList_reset ipsType_large'>
					<li>
						<i class='fa fa-pencil ipsType_veryLarge'></i> <span class='ipsType_light'>pencil</span>
					</li>
				</ul>
				<hr class='ipsHr'>

				<h3 class='ipsType_reset'>Reverting</h3>
				<ul class='ipsList_reset ipsType_large'>
					<li>
						<i class='fa fa-undo ipsType_veryLarge'></i> <span class='ipsType_light'>undo</span>
					</li>
				</ul>
				<hr class='ipsHr'>

				<h3 class='ipsType_reset'>Go Somewhere</h3>
				<ul class='ipsList_reset ipsType_large'>
					<li>
						<i class='fa fa-arrow-right ipsType_veryLarge'></i> <span class='ipsType_light'>arrow-right</span>
					</li>
				</ul>
				<hr class='ipsHr'>

				<h3 class='ipsType_reset'>Open External Link</h3>
				<ul class='ipsList_reset ipsType_large'>
					<li>
						<i class='fa fa-external-link ipsType_veryLarge'></i> <span class='ipsType_light'>external-link</span>
					</li>
				</ul>
				<hr class='ipsHr'>

				<h3 class='ipsType_reset'>Confirming Yes/No</h3>
				<ul class='ipsList_reset ipsType_large'>
					<li>
						<i class='fa fa-check ipsType_veryLarge'></i> <span class='ipsType_light'>check</span>
					</li>
					<li>
						<i class='fa fa-times ipsType_veryLarge'></i> <span class='ipsType_light'>times</span>
					</li>
				</ul>
				<hr class='ipsHr'>

				<h3 class='ipsType_reset'>Permissions</h3>
				<ul class='ipsList_reset ipsType_large'>
					<li>
						<i class='fa fa-lock ipsType_veryLarge'></i> <span class='ipsType_light'>lock</span>
					</li>
					<li>
						<i class='fa fa-unlock ipsType_veryLarge'></i> <span class='ipsType_light'>unlock</span>
					</li>
				</ul>
				<hr class='ipsHr'>

				<h3 class='ipsType_reset'>Log In/Sign In</h3>
				<ul class='ipsList_reset ipsType_large'>
					<li>
						<i class='fa fa-key ipsType_veryLarge'></i> <span class='ipsType_light'>key</span>
					</li>
				</ul>
				<hr class='ipsHr'>

				<h3 class='ipsType_reset'>Copy</h3>
				<ul class='ipsList_reset ipsType_large'>
					<li>
						<i class='fa fa-copy ipsType_veryLarge'></i> <span class='ipsType_light'>copy</span>
					</li>
				</ul>
				<hr class='ipsHr'>

				<h3 class='ipsType_reset'>Settings</h3>
				<ul class='ipsList_reset ipsType_large'>
					<li>
						<i class='fa fa-cog ipsType_veryLarge'></i> <span class='ipsType_light'>cog</span>
					</li>
				</ul>
			</div>
			<div class='ipsGrid_span6'>
				<h3 class='ipsType_reset'>Flagging On/Off</h3>
				<ul class='ipsList_reset ipsType_large'>
					<li>
						<i class='fa fa-flag ipsType_veryLarge'></i> <span class='ipsType_light'>flag</span>
					</li>
					<li>
						<i class='fa fa-flag-o ipsType_veryLarge'></i> <span class='ipsType_light'>flag-o</span>
					</li>
				</ul>
				<hr class='ipsHr'>

				<h3 class='ipsType_reset'>Star On/Off</h3>
				<ul class='ipsList_reset ipsType_large'>
					<li>
						<i class='fa fa-star ipsType_veryLarge'></i> <span class='ipsType_light'>star</span>
					</li>
					<li>
						<i class='fa fa-star-o ipsType_veryLarge'></i> <span class='ipsType_light'>star-o</span>
					</li>
				</ul>
				<hr class='ipsHr'>

				<h3 class='ipsType_reset'>Developer/Application</h3>
				<ul class='ipsList_reset ipsType_large'>
					<li>
						<i class='fa fa-cogs ipsType_veryLarge'></i> <span class='ipsType_light'>cogs</span>
					</li>
				</ul>
				<hr class='ipsHr'>

				<h3 class='ipsType_reset'>Help</h3>
				<ul class='ipsList_reset ipsType_large'>
					<li>
						<i class='fa fa-question-circle ipsType_veryLarge'></i> <span class='ipsType_light'>question-circle</span>
					</li>
				</ul>
				<hr class='ipsHr'>

				<h3 class='ipsType_reset'>Merge</h3>
				<ul class='ipsList_reset ipsType_large'>
					<li>
						<i class='fa fa-level-up ipsType_veryLarge'></i> <span class='ipsType_light'>level-up</span>
					</li>
				</ul>
				<hr class='ipsHr'>

				<h3 class='ipsType_reset'>Code/PHP/HTML</h3>
				<ul class='ipsList_reset ipsType_large'>
					<li>
						<i class='fa fa-code ipsType_veryLarge'></i> <span class='ipsType_light'>code</span>
					</li>
				</ul>
				<hr class='ipsHr'>

				<h3 class='ipsType_reset'>Mail/Send Mail</h3>
				<ul class='ipsList_reset ipsType_large'>
					<li>
						<i class='fa fa-envelope-o ipsType_veryLarge'></i> <span class='ipsType_light'>envelope-o</span>
					</li>
				</ul>
				<hr class='ipsHr'>

				<h3 class='ipsType_reset'>Search</h3>
				<ul class='ipsList_reset ipsType_large'>
					<li>
						<i class='fa fa-search ipsType_veryLarge'></i> <span class='ipsType_light'>search</span>
					</li>
				</ul>
				<hr class='ipsHr'>

				<h3 class='ipsType_reset'>View</h3>
				<ul class='ipsList_reset ipsType_large'>
					<li>
						<i class='fa fa-search ipsType_veryLarge'></i> <span class='ipsType_light'>search</span>
					</li>
				</ul>
				<hr class='ipsHr'>

				<h3 class='ipsType_reset'>Refresh/Reload</h3>
				<ul class='ipsList_reset ipsType_large'>
					<li>
						<i class='fa fa-refresh ipsType_veryLarge'></i> <span class='ipsType_light'>refresh</span>
					</li>
				</ul>
				<hr class='ipsHr'>

				<h3 class='ipsType_reset'>Execute/Run Now</h3>
				<ul class='ipsList_reset ipsType_large'>
					<li>
						<i class='fa fa-play-circle ipsType_veryLarge'></i> <span class='ipsType_light'>play-circle</span>
					</li>
				</ul>
				<hr class='ipsHr'>

				<h3 class='ipsType_reset'>Easy Mode/Visual Editor</h3>
				<ul class='ipsList_reset ipsType_large'>
					<li>
						<i class='fa fa-magic ipsType_veryLarge'></i> <span class='ipsType_light'>magic</span>
					</li>
				</ul>
			</div>
		</div>
	</section>
</article>