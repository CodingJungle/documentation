<ips:template parameters="" />

<div class='ipsPageHeader ipsAreaBackground ipsPad ipsClearfix ipsSpacer_bottom'>
	<h1 class='ipsType_pageTitle'>Forms</h1>
	<p class='ipsPageHeader_info'>
		Classes for building forms.
	</p>
	<p class='ipsType_reset ipsType_light'>
		See also: <a href='{url="app=documentation&module=documentation&controller=javascript&do=forms"}'>Javascript: Form Handling</a>
	</p>
</div>
<br>

<article class='ipsType_normal ipsType_richText'>

	<strong>In this section:</strong>
	<ul class='ipsList_inline'>
		<li><a href='#usage'>Usage</a></li>
		<li><a href='#layouts'>Form Layouts</a></li>
		<li><a href='#rows'>Field Rows</a></li>
		<li><a href='#required'>Required Rows</a></li>
		<li><a href='#descriptions'>Field Descriptions</a></li>
		<li><a href='#checks'>Checkboxes/Radios</a></li>
		<li><a href='#horizontal'>Horizontally-grouped Checkboxes</a></li>
		<li><a href='#vertical'>Vertically-grouped Checkboxes</a></li>
		<li><a href='#loading'>Field Loading State</a></li>
		<li><a href='#grouping'>Grouping Field Rows</a></li>
		<li><a href='#fieldsets'>Fieldsets</a></li>
		<li><a href='#example'>Complete Form Example</a></li>
	</ul>
	<hr class='ipsHr'>
	<br>

	<section>
		<p>The form module provides classes for styling forms throughout the suite, with a range of options available to change the appearance and flow.</p>
		<a id='usage'></a>
		<h2>Usage</h2>
		<p>A form should have the base class <code>ipsForm</code>. In many cases this will be directly on the <code>&lt;form&gt;</code> element, but it can actually appear on any element that contains form elements. The recommended basic DOM structure for a form is as follows:</p>

		<pre class='prettyprint'>
&lt;form class='<strong>ipsForm</strong>'&gt;
	&lt;ul&gt;
		&lt;li class='<strong>ipsFieldRow</strong>'&gt;
			...
		&lt;/li&gt;
		&lt;li class='ipsFieldRow'&gt;
			...
		&lt;/li&gt;
	&lt;/ul&gt;
&lt;/form&gt;
</pre>
		<p>
			In this structure, each field row within the form appears as a <code>&lt;li&gt;</code> element with the class <code>ipsFieldRow</code>.
		</p>
	</section>

	<section>
		<a id='layouts'></a>
		<h2>Form layouts</h2>

		<p>There are two layout options for forms: vertical or horizontal. In a vertical form, field labels are displayed above the field element. In horizontal forms, the label appears to the left of the field element. The layout can be controlled by adding the classes <code>ipsForm_vertical</code> or <code>ipsForm_horizontal</code> to the root form element, respectively.</p>

		<p>Example of both types:</p>

		<div class='ipsGrid'>
			<ul class='ipsGrid_span6 ipsForm ipsForm_vertical ipsAreaBackground_light ipsPad' style='height: 100px'>
				<li class='ipsFieldRow'>
					<label class='ipsFieldRow_label'>Vertical form</label>
					<div class='ipsFieldRow_content'>
						<input type='text'>
					</div>
				</li>
			</ul>
			<ul class='ipsGrid_span6 ipsForm ipsForm_horizontal ipsAreaBackground_light ipsPad' style='height: 100px'>
				<li class='ipsFieldRow'>
					<br>
					<label class='ipsFieldRow_label'>Horizontal form</label>
					<div class='ipsFieldRow_content'>
						<input type='text'>
					</div>
				</li>
			</ul>	
		</div>

		<p class='cNote'>On small devices and with the responsive CSS enabled, horizontal layout forms will automatically collapse to become vertical layout so that they are easily readable.</p>
	</section>

	<section>
		<a id='rows'></a>
		<h2>Field Rows</h2>

		<p>Each field row within the form has a number of options available, depending on the type of field. The basic structure for a field row is as follows:</p>

<pre class='prettyprint'>
&lt;li class='ipsFieldRow'&gt;
	&lt;label for='example' class='<strong>ipsFieldRow_label</strong>'&gt;
		Example field
	&lt;/label&gt;
	&lt;div class='<strong>ipsFieldRow_content</strong>'&gt;
		&lt;input type='text' id='example' value=''&gt;
	&lt;/div&gt;
&lt;/li&gt;
</pre>

		<p>The row receives the base class <code>ipsFieldRow</code>. Within this element are the label and content elements. The label receives the class <code>ipsFieldRow_label</code>, while the content wrapper receives the class <code>ipsFieldRow_content</code>. The content element can theoretically contain anything, though naturally it should be kept simple for best usability.</p>

		<p>There are several additional classes that can be applied to field rows.</p>

		<p>
			<code>ipsFieldRow_primary</code><br>
			A primary field row causes text inputs to be enlarged to convey importance<br><br>

			<code>ipsFieldRow_fullWidth</code><br>
			Causes appropriate form controls (primarily text-based/select inputs) to take up all available horizontal space<br><br>

			<code>ipsFieldRow_noLabel</code><br>
			If no label is needed, this class can be used to ensure spacing still works as expected. Do give thought to usability before deciding to remove a label.
		</p>
		<br>

		<a id='required'></a>
		<h3>Required fields</h3>
		<p>To add a 'required' indicator to a field, an element with the class <code>ipsFieldRow_required</code> can be added inside the label element, like so:</p>

<pre class='prettyprint'>
&lt;label for='elExample' class='ipsFieldRow_label'&gt;
	Field title &lt;span class='ipsFieldRow_required'&gt;Required&lt;/span&gt;
&lt;/label&gt;
</pre>

		<p>On horizontal-layout forms, the text inside the 'required' indicator element is automatically replaced with an asterisk in order to conserve space.</p>
		<br>

		<a id='descriptions'></a>
		<h3>Field descriptions</h3>
		<p>Field descriptions can be added immediately following the form control, inside of the field content element, by using the class <code>ipsFieldRow_desc</code>. For example:</p>

<pre class='prettyprint'>
&lt;div class='ipsFieldRow_content'&gt;
	&lt;input type='text' size='30'&gt;&lt;br&gt;
	&lt;span class='ipsFieldRow_desc'&gt;This is a field description&lt;/span&gt;
&lt;/div&gt;
</pre>
	</section>

	<section>
		<a id='checks'></a>
		<h2>Checkboxes/Radios</h2>

		<p>The markup and classes used for checkboxes or radio buttons are by necessity a little different to that described above, because typically the element will sit to the left of the label and appear inline with it.</p>
		<p>This is an example of the markup used for checkboxes or radios (note that although the class refers to 'checkbox', it can be used for both types of control):</p>

<pre class='prettyprint'>
&lt;li class='ipsFieldRow ipsFieldRow_checkbox'&gt;
	&lt;input type='checkbox' id='elExampleCheckbox'&gt; 
	&lt;div class='ipsFieldRow_content'&gt;
		&lt;label for='elExampleCheckbox'&gt;Remember me&lt;/label&gt;
	&lt;/div&gt;
&lt;/li&gt;
</pre>

		<p>There are a couple of differences here. Firstly, the class <code>ipsFieldRow_checkbox</code> has been added to the field row to denote that this row should be handled differently. Secondly, the checkbox control now sits as a direct descendant of the row element, while the label moves inside the content element.</p>

		<p>This example renders as follows:</p>

		<div class='ipsAreaBackground ipsPad cExample'>
			<ul class='ipsForm ipsForm_vertical'>
				<li class='ipsFieldRow ipsFieldRow_checkbox'>
					<input type='checkbox' id='elExampleCheckbox'> 
					<div class='ipsFieldRow_content'>
						<label for='elExampleCheckbox'>Remember me</label>
					</div>
				</li>
			</ul>
		</div>
		<br>

		<a id='horizontal'></a>
		<h3>Horizontally-grouped checkboxes</h3>

		<p>Another common way to display checkbox/radio controls is as a horizontal list of choices. This can be done with the following markup:</p>

<pre class='prettyprint'>
&lt;li class='ipsFieldRow'&gt;
	&lt;span class='ipsFieldRow_label'&gt;Choose an option&lt;/span&gt;
	&lt;ul class='ipsFieldRow_content ipsList_reset'&gt;
		&lt;li class='<strong>ipsFieldRow_inlineCheckbox</strong>'&gt;
			&lt;input type='radio' id='checkbox1'&gt; &lt;label for='checkbox1'&gt;Option 1&lt;/label&gt;
		&lt;/li&gt;
		&lt;li class='ipsFieldRow_inlineCheckbox'&gt;
			&lt;input type='radio' id='checkbox2'&gt; &lt;label for='checkbox2'&gt;Option 2&lt;/label&gt;
		&lt;/li&gt;
		&lt;li class='ipsFieldRow_inlineCheckbox'&gt;
			&lt;input type='radio' id='checkbox3'&gt; &lt;label for='checkbox3'&gt;Option 3&lt;/label&gt;
		&lt;/li&gt;
	&lt;/ul&gt;
&lt;/li&gt;
</pre>

		<p>Here we're building the field row content element as a list (we use <code>ipsList_reset</code> to remove margins and padding), with each list item receiving the class <code>ipsFieldRow_inlineCheckbox</code> to align them horizontally.</p>
		<p>The above example produces the following result:</p>

		<div class='ipsAreaBackground ipsPad cExample'>
			<ul class='ipsForm ipsForm_vertical'>
				<li class='ipsFieldRow'>
					<span class='ipsFieldRow_label'>Choose an option</span>
					<ul class='ipsFieldRow_content ipsList_reset'>
						<li class='ipsFieldRow_inlineCheckbox'>
							<input type='radio' id='checkbox1'> <label for='checkbox1'>Option 1</label>
						</li>
						<li class='ipsFieldRow_inlineCheckbox'>
							<input type='radio' id='checkbox2'> <label for='checkbox2'>Option 2</label>
						</li>
						<li class='ipsFieldRow_inlineCheckbox'>
							<input type='radio' id='checkbox3'> <label for='checkbox3'>Option 3</label>
						</li>
					</ul>
				</li>
			</ul>
		</div>
		<br>

		<a id='vertical'></a>
		<h3>Vertically-grouped checkboxes</h3>

		<p>You can also group checkboxes and radio controls in a vertical list. The markup looks like this:</p>

		<pre class='prettyprint'>
&lt;li class='ipsFieldRow'&gt;
	&lt;span class='ipsFieldRow_label'&gt;Choose an option&lt;/span&gt;
	&lt;ul class='ipsFieldRow_content <strong>ipsField_fieldList</strong>'&gt;
		&lt;li&gt;
			&lt;input type='checkbox' id='option1'&gt;
			&lt;div class='<strong>ipsField_fieldList_content</strong>'&gt;
				&lt;label for='option1'&gt;Option 1&lt;/label&gt;&lt;br&gt;
				&lt;span class='ipsFieldRow_desc'&gt;Option 1 description&lt;/span&gt;
			&lt;/div&gt;
		&lt;/li&gt;
		&lt;li&gt;
			&lt;input type='checkbox' id='option2'&gt;
			&lt;div class='ipsField_fieldList_content'&gt;
				&lt;label for='option1'&gt;Option 2&lt;/label&gt;&lt;br&gt;
				&lt;span class='ipsFieldRow_desc'&gt;Option 2 description&lt;/span&gt;
			&lt;/div&gt;
		&lt;/li&gt;
	&lt;/ul&gt;
&lt;/li&gt;
</pre>

		<p>Which renders as:</p>

		<div class='ipsAreaBackground ipsPad cExample'>
			<ul class='ipsForm ipsForm_vertical'>
				<li class='ipsFieldRow'>
					<span class='ipsFieldRow_label'>Choose an option</span>
					<ul class='ipsFieldRow_content ipsField_fieldList'>
						<li>
							<input type='checkbox' id='option1'>
							<div class='ipsField_fieldList_content'>
								<label for='option1'>Option 1</label><br>
								<span class='ipsFieldRow_desc'>Option 1 description</span>
							</div>
						</li>
						<li>
							<input type='checkbox' id='option2'>
							<div class='ipsField_fieldList_content'>
								<label for='option1'>Option 2</label><br>
								<span class='ipsFieldRow_desc'>Option 2 description</span>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</section>

	<section>
		<a id='loading'></a>
		<h2>Field loading state</h2>

		<p>Text-based form inputs (text inputs, date fields, textareas, etc.) can be shown in a loading state by adding the class <code>ipsField_loading</code> (usually with Javascript). This causes the field to show a loading throbber in the field.</p>

		<div class='ipsAreaBackground ipsPad cExample'>
			<ul class='ipsForm ipsForm_horizontal'>
				<li class='ipsFieldRow'>
					<label class='ipsFieldRow_label'>Loading example</label>
					<div class='ipsFieldRow_content'>
						<input type='text' class='ipsField_loading' placeholder='This field is loading' size='30'>
					</div>
				</li>
			</ul>
		</div>

		<p class='cNote'>
			Note: this effect is achieved using background images. If you define styles for form fields that specify a background image, the loading effect may not render correctly.
		</p>
	</section>

	<section>
		<a id='grouping'></a>
		<h2>Grouping field rows</h2>

		<h3>Adding section headers</h3>

		<p>You can add a section header to a form by adding a header element of your choice with the class <code>ipsFieldRow_section</code>, like this:</p>

		<pre class='prettyprint'>
&lt;li&gt;
	&lt;h2 class='ipsFieldRow_section'&gt;A section header&lt;/h2&gt;
&lt;/li&gt;
</pre>
		<br>

		<a id='fieldsets'></a>
		<h3>Fieldsets</h3>

		<p>Fields can be grouped together in related sets by using fieldsets with the class <code>ipsForm_group</code>. The markup for this kind of structure looks like this:</p>

		<pre class='prettyprint'>
&lt;div class='ipsForm ipsForm_horizontal'&gt;
	<strong>&lt;fieldset class='ipsForm_group'&gt;</strong>
		&lt;legend class='<strong>ipsForm_groupTitle</strong>'&gt;First group&lt;/legend&gt;
		&lt;ul class='<strong>ipsForm_groupContent</strong>'&gt;
			&lt;li class='ipsFieldRow'&gt;
				&lt;!-- Field row content --&gt;
			&lt;/li&gt;
			&lt;li class='ipsFieldRow'&gt;
				&lt;!-- Field row content --&gt;
			&lt;/li&gt;
		&lt;/ul&gt;
	&lt;/fieldset&gt;

	&lt;fieldset class='ipsForm_group'&gt;
		&lt;legend class='ipsForm_groupTitle'&gt;Second group&lt;/legend&gt;
		&lt;ul class='ipsForm_groupContent'&gt;
			&lt;li class='ipsFieldRow'&gt;
				&lt;!-- Field row content --&gt;
			&lt;/li&gt;
			&lt;li class='ipsFieldRow'&gt;
				&lt;!-- Field row content --&gt;
			&lt;/li&gt;
		&lt;/ul&gt;
	&lt;/fieldset&gt;
&lt;/div&gt;
</pre>
		
		<p>This produces (with field row content added):</p>

		<div class='ipsAreaBackground_light ipsPad cExample'>
			<div class='ipsForm ipsForm_horizontal'>
				<fieldset class='ipsForm_group'>
					<legend class='ipsForm_groupTitle'>First group</legend>
					<ul class='ipsForm_groupContent'>
						<li class='ipsFieldRow'>
							<label class='ipsFieldRow_label'>Field title</label>
							<div class='ipsFieldRow_content'>
								<input type='text' size='30'>
							</div>
						</li>
						<li class='ipsFieldRow'>
							<label class='ipsFieldRow_label'>Field title</label>
							<div class='ipsFieldRow_content'>
								<input type='text' size='30'>
							</div>
						</li>
					</ul>
				</fieldset>

				<fieldset class='ipsForm_group'>
					<legend class='ipsForm_groupTitle'>Second group</legend>
					<ul class='ipsForm_groupContent'>
						<li class='ipsFieldRow'>
							<label class='ipsFieldRow_label'>Field title</label>
							<div class='ipsFieldRow_content'>
								<input type='text' size='30'>
							</div>
						</li>
						<li class='ipsFieldRow'>
							<label class='ipsFieldRow_label'>Field title</label>
							<div class='ipsFieldRow_content'>
								<input type='text' size='30'>
							</div>
						</li>
					</ul>
				</fieldset>
			</div>
		</div>
		
		<p>The class <code>ipsForm_group</code> is added to a container element - this will typically be a fieldset. Within that element will be a title element, with the class <code>ipsForm_groupTitle</code>. This too can be any element, but a <code>legend</code> element will usually make most sense. Finally, there's an element with the class <code>ipsForm_groupContent</code> which wraps all of the field rows in the group.</p>
	</section>

	<section>
		<a id='example'></a>
		<h2>Complete form example</h2>

		<p>Here is a complete example of a form, which can be toggled between vertical and horizontal layouts for demonstration purposes. The markup for this example follows after.</p>
		<p><a href='#' id='toggle_form'><i class='fa fa-exchange'></i> Toggle form layout</a></p>
		

		<div class='ipsAreaBackground_light ipsPad'>
			<ul class='ipsForm ipsForm_horizontal' id='form_example'>
				<li class='ipsFieldRow ipsFieldRow_primary ipsFieldRow_fullWidth'>
					<label class='ipsFieldRow_label'>Name <span class='ipsFieldRow_required'>Required</span></label>
					<div class='ipsFieldRow_content'>
						<input type='text' size='30' placeholder='First name'>
					</div>
				</li>
				<li class='ipsFieldRow'>
					<label class='ipsFieldRow_label'>E-mail Address <span class='ipsFieldRow_required'>Required</span></label>
					<div class='ipsFieldRow_content'>
						<input type='text' size='30' placeholder='example@me.com'><br>
						<span class='ipsFieldRow_desc'>We keep this confidential</span>
					</div>
				</li>
				<li class='ipsFieldRow'>
					<span class='ipsFieldRow_label'>Please send me</span>
					<div class='ipsFieldRow_content'>
						<label class='ipsFieldRow_inlineCheckbox'><input type='checkbox'> Updates</label>
						<label class='ipsFieldRow_inlineCheckbox'><input type='checkbox'> Offers</label>
					</div>
				</li>
				<li class='ipsFieldRow ipsFieldRow_checkbox'>
					<input type='checkbox'>
					<div class='ipsFieldRow_content'>
						<label class='ipsFieldRow_label'><strong>I agree to the terms and conditions</strong></label>
					</div>
				</li>
				<li class='ipsFieldRow'>
					<div class='ipsFieldRow_content'>
						<button type='submit' class='ipsButton ipsButton_medium ipsButton_primary'>Submit</button>
					</div>
				</li>
			</ul>
		</div>
		<br>
		<script type='text/javascript'>
			$('#toggle_form').click( function (e) {
				e.preventDefault();

				var form = $('#form_example');

				if( form.hasClass('ipsForm_horizontal') ){
					form.removeClass('ipsForm_horizontal').addClass('ipsForm_vertical');
				} else {
					form.addClass('ipsForm_horizontal').removeClass('ipsForm_vertical');
				}
			});
		</script>

<pre class='prettyprint'>
&lt;ul class='ipsForm ipsForm_horizontal' id='form_example'&gt;
	&lt;li class='ipsFieldRow ipsFieldRow_primary ipsFieldRow_fullWidth'&gt;
		&lt;label class='ipsFieldRow_label'&gt;Name &lt;span class='ipsFieldRow_required'&gt;Required&lt;/span&gt;&lt;/label&gt;
		&lt;div class='ipsFieldRow_content'&gt;
			&lt;input type='text' size='30' placeholder='First name'&gt;
		&lt;/div&gt;
	&lt;/li&gt;
	&lt;li class='ipsFieldRow'&gt;
		&lt;label class='ipsFieldRow_label'&gt;E-mail Address &lt;span class='ipsFieldRow_required'&gt;Required&lt;/span&gt;&lt;/label&gt;
		&lt;div class='ipsFieldRow_content'&gt;
			&lt;input type='text' size='30' placeholder='example@me.com'&gt;&lt;br&gt;
			&lt;span class='ipsFieldRow_desc'&gt;We keep this confidential&lt;/span&gt;
		&lt;/div&gt;
	&lt;/li&gt;
	&lt;li class='ipsFieldRow'&gt;
		&lt;span class='ipsFieldRow_label'&gt;Please send me&lt;/span&gt;
		&lt;div class='ipsFieldRow_content'&gt;
			&lt;label class='ipsFieldRow_inlineCheckbox'&gt;&lt;input type='checkbox'&gt; Updates&lt;/label&gt;
			&lt;label class='ipsFieldRow_inlineCheckbox'&gt;&lt;input type='checkbox'&gt; Offers&lt;/label&gt;
		&lt;/div&gt;
	&lt;/li&gt;
	&lt;li class='ipsFieldRow ipsFieldRow_checkbox'&gt;
		&lt;input type='checkbox'&gt;
		&lt;div class='ipsFieldRow_content'&gt;
			&lt;label class='ipsFieldRow_label'&gt;&lt;strong&gt;I agree to the terms and conditions&lt;/strong&gt;&lt;/label&gt;
		&lt;/div&gt;
	&lt;/li&gt;
	&lt;li class='ipsFieldRow'&gt;
		&lt;div class='ipsFieldRow_content'&gt;
			&lt;input type='submit' class='ipsButton ipsButton_primary' value='Submit'&gt;
		&lt;/div&gt;
	&lt;/li&gt;
&lt;/ul&gt;
</pre>
	</section>
</article>