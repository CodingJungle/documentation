<ips:template parameters="" />

<div class='ipsPageHeader ipsAreaBackground ipsPad ipsClearfix ipsSpacer_bottom'>
	<h1 class='ipsType_pageTitle'>Images &amp; Photos</h1>
	<p class='ipsPageHeader_info'>
		Classes for displaying images, like user photos and thumbnails.
	</p>
</div>
<br>

<article class='ipsType_normal ipsType_richText'>

	<strong>In this section:</strong>
	<ul class='ipsList_inline'>
		<li><a href='#usage'>Usage</a></li>
		<li><a href='#wrapping'>Wrapping Grid Items</a></li>
		<li><a href='#collapsing'>Collapsing on Smaller Devices</a></li>
	</ul>
	<hr class='ipsHr'>
	<br>

	<section>
		<h2>User profile photos</h2>
		<p>When displaying a user profile photo, the markup will usually look something like this:</p>
		
<pre class='prettyprint'>
&lt;a href='#' class='ipsUserPhoto ipsUserPhoto_mini'&gt;
	&lt;img src='...' alt=''&gt;
&lt;/a&gt;
</pre>

		<p>The image tag itself should be contained within another element - usually a link. It is this wrapper element that receives the <code>ipsUserPhoto</code> base class, and one of the following sizing classes:</p>

		<ul class='ipsList_bullets'>
			<li><code>ipsUserPhoto_tiny</code> - 30 pixels square</li>
			<li><code>ipsUserPhoto_mini</code> - 40 pixels square</li>
			<li><code>ipsUserPhoto_small</code> - 50 pixels square</li>
			<li><code>ipsUserPhoto_medium</code> - 75 pixels square</li>
			<li><code>ipsUserPhoto_large</code> - 90 pixels square</li>
			<li><code>ipsUserPhoto_xlarge</code> - 120 pixels square</li>
		</ul>

		<div class='ipsAreaBackground ipsPad ipsType_light cExample'>
			<ul class='ipsList_inline'>
				<li class='ipsType_center'>
					<a href='#' class='ipsUserPhoto ipsUserPhoto_tiny'><img src='http://i.imgur.com/LU26oHo.jpg' alt=''></a><br>
					Tiny
				</li>
				<li class='ipsType_center'>
					<a href='#' class='ipsUserPhoto ipsUserPhoto_mini'><img src='http://i.imgur.com/LU26oHo.jpg' alt=''></a><br>
					Mini
				</li>
				<li class='ipsType_center'>
					<a href='#' class='ipsUserPhoto ipsUserPhoto_small'><img src='http://i.imgur.com/LU26oHo.jpg' alt=''></a><br>
					Small
				</li>
				<li class='ipsType_center'>
					<a href='#' class='ipsUserPhoto ipsUserPhoto_medium'><img src='http://i.imgur.com/LU26oHo.jpg' alt=''></a><br>
					Medium
				</li>
				<li class='ipsType_center'>
					<a href='#' class='ipsUserPhoto ipsUserPhoto_large'><img src='http://i.imgur.com/LU26oHo.jpg' alt=''></a><br>
					Large
				</li>
				<li class='ipsType_center'>
					<a href='#' class='ipsUserPhoto ipsUserPhoto_xlarge'><img src='http://i.imgur.com/LU26oHo.jpg' alt=''></a><br>
					Extra Large
				</li>
			</ul>
		</div>

		<p class='cNote'>
			Note: The default theme displays user photos with rounded edges. This is a theme setting, <code>rounded_photos</code>, and can be changed via the theme settings panel per-theme.
		</p>
	</section>

	<section>
		<h2>Photo panels</h2>

		<p>A photo panel allows you to display an image to the left of text and other elements, with the text having a sufficient margin such that it doesn't wrap around the image itself. The basic structure is:</p>

<pre class='prettyprint'>
&lt;div class='ipsPhotoPanel ipsPhotoPanel_mini'&gt;
	&lt;a href='#' class='<strong>ipsUserPhoto ipsUserPhoto_mini</strong>'&gt;&lt;img src='...' alt=''&gt;&lt;/a&gt;
	&lt;div&gt;
		&lt;h2 class='ipsType_sectionHead'&gt;Some title&lt;/h2&gt;
		&lt;p class='ipsType_reset'&gt;
			Nulla vitae elit libero, a pharetra augue. Aenean lacinia bibendum nulla sed consectetur.
		&lt;/p&gt;
	&lt;/div&gt;
&lt;/div&gt;
</pre>

		<p>This would render:</p>

		<div class='ipsAreaBackground_light ipsPad cExample'>
			<div class='ipsPhotoPanel ipsPhotoPanel_mini'>
				<a href='#' class='ipsUserPhoto ipsUserPhoto_mini'><img src='http://i.imgur.com/LU26oHo.jpg' alt=''></a>
				<div>
					<h2 class='ipsType_sectionHead'>Some title</h2>
					<p class='ipsType_reset'>Nulla vitae elit libero, a pharetra augue. Aenean lacinia bibendum nulla sed consectetur.</p>
				</div>
			</div>
		</div>
	
		<p>The wrapper receives the base class <code>ipsPhotoPanel</code>, and one of the sizing classes:</p>

		<ul class='ipsList_bullets'>
			<li><code>ipsPhotoPanel_tiny</code></li>
			<li><code>ipsPhotoPanel_mini</code></li>
			<li><code>ipsPhotoPanel_small</code></li>
			<li><code>ipsPhotoPanel_medium</code></li>
			<li><code>ipsPhotoPanel_large</code></li>
		</ul>

		<p>The sizing class should match the size of the photo contained within (e.g. if the photo has the class <code>ipsUserPhoto_small</code>, the photo panel should use the size class <code>ipsPhotoPanel_small</code>).</p>
		<br>
		
		<h3>Responsive photo panels <span class='ipsBadge ipsBadge_style3'>Responsive</span></h3>		

		<p>By default, photo panels will wrap on mobile devices. Should you want to instead hide the photo on those devices, you can add one of the following classes to the wrapper:</p>

		<ul class='ipsList_bullets'>
			<li><code>ipsPhotoPanel_notTablet</code> - Hide photo on tablet <em>and</em> phone devices</li>
			<li><code>ipsPhotoPanel_notPhone</code> - Hide photo on phone devices only</li>
		</ul>
	</section>


	<section>
		<h2>General images</h2>

		<p>Two classes are available for general user-posted images.</p>

		<p>
			<code>ipsImage</code><br>
			Can be applied to any image tag. Adds a small amount of padding and a border, and ensures that the image scales properly on smaller devices.
			<br><br>
			<code>ipsImage_thumb</code><br>
			For use with image thumbnails, and used in conjunction with the above class. Sets a maximum width and height of 175 pixels. 
		</p>
	</section>

	<section>
		<h2>'Default' images</h2>
		<p>A class is provided for 'default' or 'missing' images - such as those where a real thumbnail is not available. To use it, simply apply the class <code>ipsNoThumb</code> to an empty block element. You can add additional styles if necessary to change the sizing to suit your use-case.</p>

		<div class='ipsAreaBackground ipsPad cExample'>
			<div class='ipsNoThumb' style='height: 100px; width: 100px'></div>
		</div>
	</section>

</article>