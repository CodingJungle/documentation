<ips:template parameters="" />

<div class='ipsPageHeader ipsAreaBackground ipsPad ipsClearfix ipsSpacer_bottom'>
	<h1 class='ipsType_pageTitle'>Data Lists</h1>
	<p class='ipsPageHeader_info'>
		Classes for building complex lists containing multiple types of data, which can be displayed responsively.
	</p>
</div>
<br>

<article class='ipsType_normal ipsType_richText'>

	<strong>In this section:</strong>
	<ul class='ipsList_inline'>
		<li><a href='#usage'>Usage</a></li>
		<li><a href='#spacing'>Row Spacing</a></li>
		<li><a href='#zebra'>Zebra Striping</a></li>
		<li><a href='#main'>Main Content Cell</a></li>
		<li><a href='#stats'>Statistics Cell</a></li>
		<li><a href='#last'>Last Poster Cell</a></li>
		<li><a href='#stats'>Generic Cells</a></li>
		<li><a href='#other'>Other Cell Types</a></li>
		<li><a href='#coloring'>Coloring Rows</a></li>
		<li><a href='#example'>Putting It Together</a></li>
	</ul>
	<hr class='ipsHr'>
	<br>
	
	<section>
		<p>The data list CSS module allows us to display complex lists, while rearranging the display in situations that demand it, such as on small devices.</p>
	</section>

	<section>
		<a id='usage'></a>
		<h2>Usage</h2>

		<p>A data list consists of a wrapper and individual list items, which can contain various types of data. In most cases, a data list would use one of the HTML list elements (such as <code>ul</code> or <code>ol</code>) for semantic purposes, though this isn't necessarily required.</p>

		<p>The basic markup structure is as follows:</p>

<pre class='prettyprint'>
&lt;ol class='<strong>ipsDataList</strong>' itemscope itemtype="http://schema.org/ItemList"&gt;
	&lt;meta itemprop="itemListOrder" content="Descending"&gt;
	&lt;li class='<strong>ipsDataItem ipsDataItem_unread</strong>' itemprop="itemListElement"&gt;
		...
	&lt;/li&gt;
	&lt;li class='ipsDataItem' itemprop="itemListElement"&gt;
		...
	&lt;/li&gt;
	&lt;li class='ipsDataItem' itemprop="itemListElement"&gt;
		...
	&lt;/li&gt;
&lt;/ol&gt;
</pre>

		<p>The root element should be given the classname <code>ipsDataList</code>. Individual list items receive the classname <code>ipsDataItem</code>. To indicate unread status within a row, add the class <code>ipsDataItem_unread</code> to the list item. The other elements in the row may style differently in 'unread' rows to indicate that status.</p>

		<p>Within the <code>ipsDataItem</code>, multiple other elements can exist which (at desktop resolution) represent cells of data. On smaller devices, these elements reflow to give a more suitable layout. These elements are outlined below. An example of a data list row showing many of these options is shown here:</p>

		<div class='ipsAreaBackground_light ipsPad_half ipsType_medium'>
			<ol class='ipsDataList'>
				<li class='ipsDataItem ipsClearfix ipsDataItem_unread' data-forumid='2'>
					<div class='ipsDataItem_icon'>
						<a class='cForumIcon cForumIcon_unread ipsType_large' href='#'><i class='fa fa-comments'></i></a>
					</div>
					<div class='ipsDataItem_main'>
						<h4 class='ipsDataItem_title'><a href='#'>Pre-sales questions</a></h4>
						<p class='ipsDataItem_meta'>
							Question before you purchase? Post here and get help from both IPS and from other clients. You can also email sales@invisionpower.com for assistance.
						</p>
						<ul class='ipsDataItem_subList ipsList_inline'>
							<li><a href='#'>Company Feedback</a></li>
							<li class='ipsDataItem_unread'><a href='#'>General Suite Feedback</a></li>
						</ul>
					</div>
					<dl class='ipsDataItem_stats ipsDataItem_statsLarge'>
						<dt class='ipsDataItem_stats_number'>18,852</dt>
						<dd class='ipsDataItem_stats_type ipsType_light'>Posts</dd>
					</dl>
					<ul class='ipsDataItem_lastPoster ipsDataItem_withPhoto'>
						<li><a href='#' class='ipsUserPhoto ipsUserPhoto_mini ipsPos_left'><img src='{member="photo"}' alt=''></a></li>
						<li><a href='#'>BBCode’s description variable</a></li>
						<li>By <a href='#'>testadmin</a></li>
						<li data-short='1 dy'><time class='ipsType_light'>Yesterday</time></li>
					</ul>
					<div class='ipsDataItem_modCheck'>
						<input type='checkbox' checked>
					</div>
				</li>
			</ol>
		</div>
	</section>

	<section>
		<a id='spacing'></a>
		<h2>Row spacing</h2>
		<p>By default, rows are comfortably spaced. An additional class <code>ipsDataList_reducedSpacing</code> is available where reduced horizontal cell padding is desired.</p>
	</section>

	<section>
		<a id='zebra'></a>
		<h2>Zebra striping</h2>
		<p>By default, zebra striping (alternate background colors) are not included on data lists. To add them, add the class <code>ipsDataList_zebra</code> to the data list wrapper.</p>
	</section>

	<section>
		<a id='main'></a>
		<h2>Main content cell - <code>ipsDataItem_main</code></h2>

		<p>Represents the primary data in the row, typically containing the row title and some metadata. Expands to take up whatever space is left by the remaining cells in the row. Example:</p>

<pre class='prettyprint'>
&lt;div class='ipsDataItem_main'&gt;
	&lt;h4 class='ipsDataItem_title'&gt;&lt;a href='#'&gt;Item title&lt;/a&gt;&lt;/h4&gt;
	&lt;p class='ipsDataItem_meta'&gt;
		By testadmin, yesterday, 2:21pm
	&lt;/p&gt;
&lt;/div&gt;
</pre>

		<p>Within this cell, <code>ipsDataItem_title</code> defines the title element, while <code>ipsDataItem_meta</code> defines some non-specific metadata associated with the title.</p>
		<p>In the main cell, a sub-list can be specified for data such as sub-forums or sub-albums. This should appear after the <code>ipsDataItem_meta</code> element. An example of the markup:</p>

<pre class='prettyprint'>
&lt;ul class='<strong>ipsDataItem_subList</strong> ipsList_inline'&gt;
	&lt;li&gt;&lt;a href='#'&gt;Sub-item 1&lt;/a&gt;&lt;/li&gt;
	&lt;li class='ipsDataItem_unread'&gt;&lt;a href='#'&gt;Sub-item 2&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;
</pre>
	</section>

	<section>
		<a id='stats'></a>
		<h2>Statistics cell - <code>ipsDataItem_stats</code></h2>

		<p>This class is applied to an element which represents statistics for this row, such as the number of replies in a topic or the number of downloads a file has. An example of its usage:</p>

<pre class='prettyprint'>
&lt;dl class='<strong>ipsDataItem_stats</strong>'&gt;
	&lt;dt class='<strong>ipsDataItem_stats_number</strong>'&gt;18,852&lt;/dt&gt;
	&lt;dd class='<strong>ipsDataItem_stats_type</strong>'&gt;Posts&lt;/dd&gt;
&lt;/dl&gt;
</pre>

		<p>The data within this element is marked up with two additional classes: </p>

		<ul class='ipsList_bullets'>
			<li><code>ipsDataItem_stats_number</code> represents the actual statistical value</li>
			<li><code>ipsDataItem_stats_type</code> represents the type of data</li>
		</ul>

		<p>The additional class <code>ipsDataItem_statsLarge</code> can be added to the element where the statistic is particularly important. This significantly increases the font size of the value to help it stand out.</p>
	</section>

	<section>
		<a id='last'></a>
		<h2>Last poster cell - <code>ipsDataItem_lastPoster</code></h2>

		<p>Used to represent the 'last poster' information, such as in a topic or other record. An example of its use:</p>

<pre class='prettyprint'>
&lt;ul class='<strong>ipsDataItem_lastPoster ipsDataItem_withPhoto</strong>'&gt;
	&lt;li&gt;&lt;img src='images/photo.jpeg'&gt;&lt;/li&gt;
	&lt;li&gt;&lt;a href='#'&gt;Username&lt;/a&gt;&lt;/li&gt;
	&lt;li <strong>data-short='1 dy'</strong>&gt;Time stamp&lt;/li&gt;
&lt;/ul&gt;
</pre>

		<p>Within this element are two or three more elements. If the <code>ipsDataItem_withPhoto</code> class is used, the first element should contain a user photo, otherwise, the first element can be omitted. The second element typically will contain the username, while the last will contain the time information.</p>

		<p>The last element contains a <code>data-short</code> attribute. This attibute should contain a very short representation of the date/time, and it is used on small devices.</p>
	</section>

	<section>
		<a id='generic'></a>
		<h2>Generic cells</h2>

		<p>A number of generic cell sizes are available for custom types of data. The cell should contain the class <code>ipsDataItem_generic</code> and <em>one</em> of the following classes:</p>

		<ul class='ipsList_bullets'>
			<li><code>ipsDataItem_size1</code> - 50px wide</li>
			<li><code>ipsDataItem_size2</code> - 75px wide</li>
			<li><code>ipsDataItem_size3</code> - 100px wide</li>
			<li><code>ipsDataItem_size4</code> - 125px wide</li>
			<li><code>ipsDataItem_size5</code> - 150px wide</li>
			<li><code>ipsDataItem_size6</code> - 175px wide</li>
			<li><code>ipsDataItem_size7</code> - 200px wide</li>
			<li><code>ipsDataItem_size8</code> - 225px wide</li>
			<li><code>ipsDataItem_size9</code> - 250px wide</li>
		</ul>
	</section>

	<section>
		<a id='other'></a>
		<h2>Other cell types</h2>

		<p>In addition to the main cell types above, there are additional cell classes that can be used.</p>

		<ul class='ipsList_bullets'>
			<li><code>ipsDataItem_icon</code> - A column which contains a status icon for the row, such as a forum icon</li>
			<li><code>ipsDataItem_modCheck</code> - A narrow column that should hold a moderator checkbox to select the row</li>
		</ul>
	</section>

	<section>
		<a id='coloring'></a>
		<h2>Coloring rows</h2>

		<p>Four additional classes are available to optionally add a background color to the row to denote status:</p>

		<ul class='ipsList_bullets'>
			<li class='ipsDataItem_new ipsPad'><code>ipsDataItem_new</code> - New items</li>
			<li class='ipsDataItem_warning ipsPad'><code>ipsDataItem_warning</code> - Items showing a warning state</li>
			<li class='ipsDataItem_error ipsPad'><code>ipsDataItem_warning</code> - Items with an error</li>
			<li class='ipsDataItem_info ipsPad'><code>ipsDataItem_info</code> - An informational state</li>
		</ul>
	</section>

	<section>
		<a id='example'></a>
		<h2>Putting it together</h2>

		<p>Here is the code for the example shown at the top of the page:</p>

<pre class='prettyprint'>
&lt;ol class='ipsDataList'&gt;
	&lt;li class='ipsDataItem ipsClearfix ipsDataItem_unread'&gt;
		&lt;div class='ipsDataItem_icon'&gt;
			&lt;i class='icon-comments'&gt;&lt;/i&gt;
		&lt;/div&gt;
		&lt;div class='ipsDataItem_main'&gt;
			&lt;h4 class='ipsDataItem_title'&gt;&lt;a href='#'&gt;Pre-sales questions&lt;/a&gt;&lt;/h4&gt;
			&lt;p class='ipsDataItem_meta'&gt;
				Question before you purchase? Post here and get help from both IPS and from other clients. You can also email sales@invisionpower.com for assistance.
			&lt;/p&gt;
			&lt;ul class='ipsDataItem_subList ipsList_inline'&gt;
				&lt;li&gt;&lt;a href='#'&gt;Company Feedback&lt;/a&gt;&lt;/li&gt;
				&lt;li class='ipsDataItem_unread'&gt;&lt;a href='#'&gt;General Suite Feedback&lt;/a&gt;&lt;/li&gt;
			&lt;/ul&gt;
		&lt;/div&gt;
		&lt;dl class='ipsDataItem_stats ipsDataItem_statsLarge'&gt;
			&lt;dt class='ipsDataItem_stats_number'&gt;18,852&lt;/dt&gt;
			&lt;dd class='ipsDataItem_stats_type ipsType_light'&gt;Posts&lt;/dd&gt;
		&lt;/dl&gt;
		&lt;ul class='ipsDataItem_lastPoster ipsDataItem_withPhoto'&gt;
			&lt;li&gt;&lt;a href='#' class='ipsUserPhoto ipsUserPhoto_mini ipsPos_left'&gt;&lt;img src='{member=&quot;photo&quot;}' alt=''&gt;&lt;/a&gt;&lt;/li&gt;
			&lt;li&gt;&lt;a href='#'&gt;BBCode’s description variable&lt;/a&gt;&lt;/li&gt;
			&lt;li&gt;By &lt;a href='#'&gt;{member=&quot;name&quot;}&lt;/a&gt;&lt;/li&gt;
			&lt;li data-short='1 dy'&gt;&lt;time class='ipsType_light'&gt;Yesterday&lt;/time&gt;&lt;/li&gt;
		&lt;/ul&gt;
		&lt;div class='ipsDataItem_modCheck'&gt;
			&lt;input type='checkbox' checked&gt;
		&lt;/div&gt;
	&lt;/li&gt;
&lt;/ol&gt;
</pre>
	</section>
</article>